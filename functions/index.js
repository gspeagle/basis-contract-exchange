import { OfferStatus } from "../src/constants/OfferStatusEnum";
import { ContractStatus } from "../src/constants/ContractStatusEnum";

const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

exports.onCreateContract = functions.firestore
  .document("users/{uid}/contracts/{contractId}")
  .onCreate(async (snap, context) => {
    const data = {
      ...snap.data(),
      status: OfferStatus.OPEN,
      createdAt: snap.createTime.seconds,
      updatedAt: snap.createTime.seconds
    };

    await snap.ref.update(data);
    await admin
      .firestore()
      .doc(`/contracts/${context.params.contractId}`)
      .set(data);
    console.log(`Created new contract! ${context.params.contractId}`);
  });

exports.onUpdateContract = functions.firestore
  .document("users/{uid}/contracts/{contractId}")
  .onUpdate(async (change, context) => {
    if (!change.before.data().createdAt) {
      return;
    }
    if (change.before.data().updatedAt === change.after.data().updatedAt) {
      const newData = {
        ...change.after.data(),
        updatedAt: change.after.updateTime._seconds
      };
      await change.after.ref.update(newData);
    } else {
      await admin
        .firestore()
        .doc(`/contracts/${context.params.contractId}`)
        .update(change.after.data());
      console.log(`Contract updated! ${context.params.contractId}`);
    }
  });

exports.onDeleteContract = functions.firestore
  .document("users/{uid}/contracts/{contractId}")
  .onDelete(async (change, context) => {
    await admin
      .firestore()
      .doc(`/contracts/${context.params.contractId}`)
      .delete();
    console.log(`Contract deleted! ${context.params.contractId}`);
  });

exports.onUpdateOffer = functions.firestore
  .document("offers/{offerId}")
  .onUpdate(async (change, context) => {
    if (!change.before.data().createdAt) {
      return;
    }
    const { offerId } = context.params;
    const data = change.after.data();
    const { status, uid, contractId } = data;
    const batch = admin.firestore().batch();
    const openOrderRef = await admin
      .firestore()
      .doc(`/users/${uid}/openOrders/${offerId}`);
    batch.delete(openOrderRef);

    if (status === OfferStatus.APPROVED) {
      const price = data.bushels * data.quantity;
      const contractData = await admin
        .firestore()
        .doc(`contracts/${contractId}`)
        .get();
      const contract = contractData.data();
      const contractRef = await admin
        .firestore()
        .doc(`users/${contract.uid}/contracts/${contractId}`);
      batch.update(contractRef, {
        status: ContractStatus.ACCEPTED,
        closedAt: change.after.updateTime,
        approvedOffer: offerId,
        price
      });
      const lastRef = await admin.firestore().doc(`/lastSold/Corn`);
      batch.update(lastRef, { price });
      const privateRef = await admin
        .firestore()
        .doc(`/users/${uid}/privateOrders/${offerId}`);
      batch.set(privateRef, data);
    } else {
      const closedRef = await admin
        .firestore()
        .doc(`/users/${uid}/closedOrders/${offerId}`);
      batch.set(closedRef, data);
    }

    await batch.commit();
  });

exports.onCreateOffer = functions.firestore
  .document("offers/{offerId}")
  .onCreate(async (snap, context) => {
    const { offerId } = context.params;
    const data = {
      ...snap.data(),
      status: OfferStatus.OPEN,
      createdAt: snap.createTime.seconds,
      updatedAt: snap.createTime.seconds
    };
    await snap.ref.update(data);
    await admin
      .firestore()
      .doc(`users/${data.uid}/openOrders/${offerId}`)
      .set(data);
    const contractData = await admin
      .firestore()
      .doc(`contracts/${data.contractId}`)
      .get();
    const contract = contractData.data();
    const price = data.bushels * data.quantity;
    const high = Math.max(contract.high || 0, price);
    const low = contract.low ? Math.min(contract.low, price) : price;
    const contractRef = await admin
      .firestore()
      .doc(`users/${contract.uid}/contracts/${data.contractId}`);
    contractRef.update({ high, low });
    console.log(`Created new offer! ${offerId}`);
  });

exports.onUpdateUser = functions.firestore
  .document("users/{uid}")
  .onUpdate(async (change, context) => {
    if (change.before.data().disabled !== change.after.data().disabled) {
      const { disabled } = change.after.data();
      const newData = {
        ...change.after.data(),
        updatedAt: change.after.updateTime._seconds,
        disabledAt: disabled ? change.after.updateTime._seconds : null
      };

      await change.after.ref.update(newData);
      await admin.auth().updateUser(context.params.uid, {
        disabled
      });
      console.log(
        `User ${context.params.uid} is ${
          disabled ? "disabled" : "  active"
        } now`
      );
    }
  });
