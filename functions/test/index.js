import { OfferStatus } from "../../src/constants/OfferStatusEnum";

const admin = require("firebase-admin");
const { assert } = require("chai");
const sinon = require("sinon");
const projectConfig = {
  projectId: "test-63223",
  databaseURL: "https://test-63223.firebaseio.com"
};
const test = require("firebase-functions-test")(projectConfig, "./key.json");
const CONTRACT_ID = "test_id";

describe("Cloud Functions", () => {
  let contractFunctions;

  before(() => {
    contractFunctions = require("../index");
  });

  after(() => {
    test.cleanup();
    return admin
      .firestore()
      .doc(`contracts/${CONTRACT_ID}`)
      .delete();
  });

  describe("Contract creation", () => {
    it('should add contract to "contracts" collection', () => {
      const updateStub = sinon.stub();
      const snap = {
        data: () => ({ testData: "bar" }),
        createTime: {
          seconds: new Date().getTime() / 1000
        },
        ref: {
          update: updateStub
        }
      };
      const expectedData = {
        ...snap.data(),
        status: OfferStatus.OPEN,
        createdAt: snap.createTime.seconds,
        updatedAt: snap.createTime.seconds
      };
      const wrapped = test.wrap(contractFunctions.onCreateContract);
      return wrapped(snap, { params: { contractId: CONTRACT_ID } }).then(
        data => {
          const firstArgument = updateStub.getCall(0).args[0];
          assert.deepEqual(
            firstArgument,
            expectedData,
            "Contract update was called with wrong arguments"
          );
          return admin
            .firestore()
            .doc(`contracts/${CONTRACT_ID}`)
            .get()
            .then(createdSnap => {
              assert.deepEqual(
                createdSnap.data(),
                expectedData,
                'Contract from "contracts" collection and "user" collects are not same'
              );
            });
        }
      );
    });
  });
});
