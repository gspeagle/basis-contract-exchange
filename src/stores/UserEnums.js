export const UserRole = {
  SUPER_ADMIN: "Super admin",
  CLIENT: "Client",
  ADMIN: "Admin",
  EMPLOYEE: "Employee",
  UNAUTHORIZED: "Unauthorized"
};
