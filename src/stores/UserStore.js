import { action, observable, computed, autorun } from "mobx";
import { defineAbilitiesFor } from "../components/ability/ability";
import { UserRole } from "./UserEnums";

class UserStore {
  constructor() {
    autorun(() => {
      defineAbilitiesFor(this);
    });
  }

  @observable user;
  @observable userRole;
  @observable isLoaded;

  @action
  setUser = user => {
    this.user = user;
    this.userRole = user ? user.role : UserRole.UNAUTHORIZED;
    this.isLoaded = true;
  };

  @computed get isLoggedIn() {
    return !!this.user;
  }
}

const store = new UserStore();
export default store;
