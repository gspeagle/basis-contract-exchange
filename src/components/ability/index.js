import { createContext } from "react";
import { createContextualCan } from "@casl/react";
import ability from "./ability";

const AbilityContext = createContext();
const Can = createContextualCan(AbilityContext.Consumer);
export { ability, AbilityContext, Can };
