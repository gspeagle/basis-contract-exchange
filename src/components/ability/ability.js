import { Ability } from "@casl/ability";
import { UserRole } from "../../stores/UserEnums";

const ability = new Ability([]);

export const defineAbilitiesFor = ({ userRole }) => {
  switch (userRole) {
    default:
    case undefined: {
      ability.update([]);
      break;
    }
    case UserRole.CLIENT: {
      ability.update([
        { actions: ["manage"], subject: "Contracts" },
        { actions: ["have"], subject: "Order" }
      ]);
      break;
    }
    case UserRole.ADMIN: {
      ability.update([
        { actions: ["create", "read", "edit"], subject: "Elevators" }
      ]);
      break;
    }
    case UserRole.EMPLOYEE: {
      ability.update([{ actions: ["read"], subject: "Elevators" }]);
      break;
    }
    case UserRole.SUPER_ADMIN: {
      ability.update([
        { actions: ["manage"], subject: "AllContracts" },
        { actions: ["manage"], subject: "AllOrders" },
        { actions: ["manage"], subject: "Users" },
        { actions: ["create", "read", "edit"], subject: "Elevators" }
      ]);
      break;
    }
  }
};

export default ability;
