import app from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

const config = {
  apiKey: "AIzaSyBE_RH6P3XwlTQ0uLAVg7XkwH4c75EftfA",
  authDomain: "test-63223.firebaseapp.com",
  databaseURL: "https://test-63223.firebaseio.com",
  projectId: "test-63223",
  storageBucket: "test-63223.appspot.com",
  messagingSenderId: "376816972692",
  appId: "1:376816972692:web:325a92d851d97a75"
};

const REACT_APP_CONFIRMATION_EMAIL_REDIRECT = "http://localhost:3001/confirm";

class Firebase {
  constructor() {
    app.initializeApp(config);
    this.emailAuthProvider = app.auth.EmailAuthProvider;
    this.auth = app.auth();
    this.db = app.firestore();
    this.firestore = app.firestore;
    this.storage = app.storage().ref();
  }

  signUpWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  signInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  signOut = () => this.auth.signOut();

  passwordReset = email => this.auth.sendPasswordResetEmail(email);

  sendEmailVerification = () =>
    this.auth.currentUser.sendEmailVerification({
      url: REACT_APP_CONFIRMATION_EMAIL_REDIRECT
    });

  passwordUpdate = password => this.auth.currentUser.updatePassword(password);

  onAuthUserListener = (next, fallback) =>
    this.auth.onAuthStateChanged(async authUser => {
      if (authUser) {
        const doc = await this.user(authUser.uid).get();
        if (doc.exists && !doc.data().disabled) {
          const dbUser = doc.data();
          authUser = {
            uid: authUser.uid,
            email: authUser.email,
            emailVerified: authUser.emailVerified,
            providerData: authUser.providerData,
            ...dbUser
          };
          next(authUser);
        } else {
          console.log(`No such document! users/${authUser.uid}`);
        }
      } else {
        fallback();
      }
    });

  user = uid => this.db.collection("users").doc(uid);
  users = () => this.db.collection("users");

  privateOrders = uid =>
    this.db
      .collection("users")
      .doc(uid)
      .collection("privateOrders");

  openOrders = uid =>
    this.db
      .collection("users")
      .doc(uid)
      .collection("openOrders");

  closedOrders = uid =>
    this.db
      .collection("users")
      .doc(uid)
      .collection("closedOrders");

  closedOrders = uid =>
    this.db
      .collection("users")
      .doc(uid)
      .collection("closedOrders");

  contract = contractId => this.db.collection("contracts").doc(contractId);
  contracts = () => this.db.collection("contracts");
  userContract = (uid, contractId) =>
    this.db
      .collection("users")
      .doc(uid)
      .collection("contracts")
      .doc(contractId);
  userContracts = uid =>
    this.db
      .collection("users")
      .doc(uid)
      .collection("contracts");
  offer = offerId => this.db.collection("offers").doc(offerId);
  lastSoldCorn = () => this.db.collection("lastSold").doc("Corn");

  elevators = () => this.db.collection(`elevators`);
  elevator = elevatorId => this.db.collection(`elevators`).doc(elevatorId);
}

export default Firebase;
