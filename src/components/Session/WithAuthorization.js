import React from "react";
import { Route, Redirect, withRouter } from "react-router-dom";
import { compose } from "recompose";
import { withFirebase } from "../Firebase";
import { inject, observer } from "mobx-react";

const withAuthorization = Component => {
  class WithAuthorization extends React.Component {
    componentDidMount() {
      this.unsubscribe = this.props.firebase.onAuthUserListener(
        user => {},
        () => {
          this.props.history.push({
            pathname: "/signIn",
            state: { from: this.props.location }
          });
        }
      );
    }

    componentWillUnmount() {
      this.unsubscribe();
    }

    render() {
      if (this.props.UserStore.isLoggedIn) {
        return <Component {...this.props} />;
      }
      return this.props.UserStore.isLoaded ? "Check auth" : "redirect";
    }
  }

  return compose(
    withRouter,
    withFirebase,
    inject("UserStore"),
    observer
  )(WithAuthorization);
};

export default withAuthorization;
