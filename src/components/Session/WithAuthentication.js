import React from "react";
import { inject } from "mobx-react";
import { compose } from "recompose";
import { withFirebase } from "../Firebase";
import UserStore from "../../stores/UserStore";

const withAuthentication = Component => {
  class WithAuthentication extends React.Component {
    constructor(props) {
      super(props);
      const user = JSON.parse(localStorage.getItem("authUser"));
      user && this.props.UserStore.setUser(user);
    }

    componentDidMount() {
      this.listener = this.props.firebase.onAuthUserListener(
        authUser => {
          //TODO anonymous
          localStorage.setItem("authUser", JSON.stringify(authUser));
          this.props.UserStore.setUser(authUser);
        },
        () => {
          localStorage.removeItem("authUser");
          this.props.UserStore.setUser(null);
        }
      );
    }

    componentWillUnmount() {
      this.listener();
    }

    render() {
      return <Component {...this.props} />;
    }
  }

  return compose(
    withFirebase,
    inject("UserStore")
  )(WithAuthentication);
};

export default withAuthentication;
