import Contract from "./Contract";
import ContractsContainer from "./ContractsContainer";
import EditContract from "./EditContract";
import NewContract from "./NewContract";

export default ContractsContainer;

export { Contract, ContractsContainer, EditContract, NewContract };
