import React, { useEffect, useState } from "react";
import * as yup from "yup";
import { Formik } from "formik";
import { StyledForm } from "../../../utils/styled";
import { LabelInput } from "../../../components/Input";
import { withFirebase } from "../../Firebase";
import { toast } from "react-toastify";
import Spinner from "../../Spinner";
import {
  basicRequiredStringValidationSchema,
  dateFutureValidationSchema,
  numberValidationSchema,
  positiveNumberValidationSchema
} from "../../../utils/validations";
import { inject } from "mobx-react";
import { compose } from "recompose";
import { categories } from "../../../utils/constants";

const FormValuesValidationSchema = yup.object().shape({
  name: basicRequiredStringValidationSchema,
  commodityName: basicRequiredStringValidationSchema,
  open: positiveNumberValidationSchema,
  deliveryDate: dateFutureValidationSchema,
  basisMonth: basicRequiredStringValidationSchema,
  basis: numberValidationSchema,
  cashPrice: positiveNumberValidationSchema,
  location: basicRequiredStringValidationSchema
});

const EditFlightForm = ({
  match: { params },
  firebase,
  history,
  location: {
    state: { contract }
  },
  UserStore
}) => {
  if (!contract) {
    toast.error("Wrong contract ID!");
    history.push("/");
  }
  return contract ? (
    <div className="flex-grow-1 d-flex flex-row justify-content-center align-items-center">
      <Formik
        enableReinitialize
        initialValues={{
          name: "",
          commodityName: "",
          open: 0,
          deliveryDate: "",
          basisMonth: "",
          basis: 0,
          cashPrice: 0,
          location: "",
          farmer: "",
          bushels: "",
          bushels500: "",
          bushelsNumber: "",
          departureLocation: "",
          ...contract
        }}
        validationSchema={FormValuesValidationSchema}
        onSubmit={async (
          formValues,
          { setSubmitting, setStatus, setErrors }
        ) => {
          setSubmitting(true);
          try {
            await firebase.userContract(contract.uid, contract.id).update({
              ...formValues
            });
            setSubmitting(false);
            toast.success("Success");
            history.push({
              pathname: `/allContracts/${contract.id}/`,
              state: {
                contract: {
                  ...contract,
                  ...formValues
                }
              }
            });
          } catch (err) {
            console.log(err);
          }
        }}
      >
        {({ errors, touched, isSubmitting, submitForm }) => (
          <StyledForm style={{ width: "100%" }}>
            <div className="row">
              <LabelInput
                type="select"
                name="category"
                label="Category"
                error={errors.category}
                touched={touched.category}
              >
                {Object.keys(categories)
                  .map(key =>
                    [
                      <option key={key} value={key} disabled>
                        {key}
                      </option>
                    ].concat(
                      categories[key].map(category => (
                        <option key={category} value={category}>
                          {category}
                        </option>
                      ))
                    )
                  )
                  .reduce((acc, categories) => acc.concat(categories))}
              </LabelInput>
              <LabelInput
                name="name"
                placeholder="Name"
                type="text"
                error={errors.name}
                touched={touched.name}
                label="Name"
              />
              <LabelInput
                name="commodityName"
                className="form-control"
                placeholder="Commodity Name"
                type="text"
                error={errors.commodityName}
                touched={touched.commodityName}
                label="Commodity Name"
              />
              <LabelInput
                name="open"
                className="form-control"
                placeholder="Open price"
                type="number"
                error={errors.open}
                touched={touched.open}
                label="Open price"
              />
              <LabelInput
                name="deliveryDate"
                className="form-control"
                placeholder="Delivery date"
                type="date"
                error={errors.deliveryDate}
                touched={touched.deliveryDate}
                label="Delivery date"
              />
              <LabelInput
                name="basisMonth"
                className="form-control"
                placeholder="Basis month"
                type="text"
                error={errors.basisMonth}
                touched={touched.basisMonth}
                label="Basis month"
              />
              <LabelInput
                name="basis"
                className="form-control"
                placeholder="Basis"
                type="number"
                error={errors.basis}
                touched={touched.basis}
                label="Basis"
              />
              <LabelInput
                name="cashPrice"
                className="form-control"
                placeholder="Cash price"
                type="number"
                error={errors.cashPrice}
                touched={touched.cashPrice}
                label="Cash price"
              />
              <LabelInput
                name="farmer"
                className="form-control"
                placeholder="Farmer"
                type="text"
                error={errors.farmer}
                touched={touched.farmer}
                label="Farmer"
              />
              <LabelInput
                name="bushels"
                className="form-control"
                placeholder="Basis Price per Bushel"
                type="number"
                error={errors.bushels}
                touched={touched.bushels}
                label="Basis Price per Bushel"
              />
              <LabelInput
                name="bushels500"
                className="form-control"
                placeholder="Basis Price Per Bushel For 500/Bu of Grain:"
                type="number"
                error={errors.bushels500}
                touched={touched.bushels500}
                label="Basis Price Per Bushel For 500/Bu of Grain:"
              />
              <LabelInput
                name="bushelsNumber"
                className="form-control"
                placeholder="The number of Bushels"
                type="number"
                error={errors.bushelsNumber}
                touched={touched.bushelsNumber}
                label="The number of Bushels"
              />
              <LabelInput
                name="departureLocation"
                className="form-control"
                placeholder="Delivery Location"
                type="text"
                error={errors.departureLocation}
                touched={touched.departureLocation}
                label="Delivery Location"
              />
              <LabelInput
                name="location"
                className="form-control"
                placeholder="Location"
                type="text"
                error={errors.location}
                touched={touched.location}
                label="Location"
              />
              <div className="col-sm-12 col-md-6 offset-md-3 my-4">
                <button
                  type="submit"
                  className="btn btn-primary btn-block button-text brown-background"
                  disabled={isSubmitting}
                  onClick={submitForm}
                >
                  Update
                </button>
              </div>
            </div>
          </StyledForm>
        )}
      </Formik>
    </div>
  ) : (
    <Spinner />
  );
};

export default compose(
  inject("UserStore"),
  withFirebase
)(EditFlightForm);
