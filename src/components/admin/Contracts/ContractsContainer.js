import React, { useEffect, useState } from "react";
import { withFirebase } from "../../Firebase";
import ContractsView from "./ContractsView";
import { compose } from "recompose";

const ContractsContainer = ({ firebase }) => {
  const [contracts, setContracts] = useState(null);
  useEffect(() => {
    return firebase.contracts().onSnapshot(querySnapshot => {
      const contracts = querySnapshot.docs.map(doc => ({
        id: doc.id,
        ...doc.data()
      }));
      setContracts(contracts);
    }, console.error);
  }, []);

  return contracts && <ContractsView contracts={contracts} />;
};

export default compose(withFirebase)(ContractsContainer);
