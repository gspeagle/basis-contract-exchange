import React from "react";
import { Button, Table } from "reactstrap";
import withAuthorization from "../../Session/WithAuthorization";
import { compose } from "recompose";
import moment from "moment";
import styled from "styled-components";
import { Link } from "react-router-dom";

const StyledTable = styled(Table)`
  margin-top: 32px;
`;

const cashBids = ({ contracts }) => {
  return (
    <>
      <StyledTable hover responsive size="sm">
        <thead>
          <tr>
            <th>User</th>
            <th>Status</th>
            <th>Category</th>
            <th>Delivery Date</th>
            <th>Basis Month</th>
            <th>Basis</th>
            <th>Cash price</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {contracts.map(data => (
            <tr key={data.id}>
              <td>{data.uid}</td>
              <td>{data.status}</td>
              <td>{data.category}</td>
              <th scope="row">
                {moment(data.deliveryDate).format("MMM DD, YYYY")}
              </th>
              <td>{data.basisMonth}</td>
              <td>{data.basis}</td>
              <td>{data.cashPrice}</td>
              <td>
                <Button
                  tag={Link}
                  to={{
                    pathname: `/allContracts/${data.id}`,
                    state: {
                      contract: { ...data }
                    }
                  }}
                  outline
                  color="primary"
                  size="sm"
                >
                  More info
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </StyledTable>
    </>
  );
};

export default compose(withAuthorization)(cashBids);
