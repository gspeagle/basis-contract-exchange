import React from "react";
import { Button, Table } from "reactstrap";
import withAuthorization from "../../Session/WithAuthorization";
import { compose } from "recompose";
import styled from "styled-components";
import { Link } from "react-router-dom";

const StyledTable = styled(Table)`
  margin-top: 32px;
`;

const cashBids = ({ users }) => {
  return (
    <>
      <StyledTable hover responsive size="sm">
        <thead>
          <tr>
            <th>id</th>
            <th>Role</th>
            <th>Email</th>
            <th>Is deleted?</th>
            <th>Last name</th>
            <th>First Name</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {users.map(data => (
            <tr key={data.id}>
              <td>{data.id}</td>
              <td>{data.role}</td>
              <td>{data.email}</td>
              <td>{data.disabled ? "Yes" : "No"}</td>
              <td>{data.lastName}</td>
              <td>{data.firstName}</td>
              <td>
                <Button
                  tag={Link}
                  to={{
                    pathname: `/users/${data.id}`,
                    state: {
                      user: { ...data }
                    }
                  }}
                  outline
                  color="primary"
                  size="sm"
                >
                  More info
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </StyledTable>
    </>
  );
};

export default compose(withAuthorization)(cashBids);
