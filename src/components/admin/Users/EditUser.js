import React from "react";
import * as yup from "yup";
import { Formik } from "formik";
import { StyledForm } from "../../../utils/styled";
import { LabelInput } from "../../../components/Input";
import { withFirebase } from "../../Firebase";
import { toast } from "react-toastify";
import {
  basicRequiredStringValidationSchema,
  emailValidationSchema,
  phoneValidationSchema,
  stringMaxValidationSchema,
  zipValidationSchema
} from "../../../utils/validations";
import { compose } from "recompose";
import { BoldSpanWithMargin } from "./UserInfo";
import { UserRole } from "../../../stores/UserEnums";
import { Button } from "reactstrap";

const FormValuesValidationSchema = yup.object().shape({
  firstName: stringMaxValidationSchema,
  role: basicRequiredStringValidationSchema,
  address: stringMaxValidationSchema,
  cellPhone: phoneValidationSchema,
  city: stringMaxValidationSchema,
  company: stringMaxValidationSchema,
  country: stringMaxValidationSchema,
  email: emailValidationSchema,
  homePhone: phoneValidationSchema,
  lastName: stringMaxValidationSchema,
  state: stringMaxValidationSchema,
  workPhone: phoneValidationSchema,
  zip: zipValidationSchema
});

const EditFlightForm = ({
  firebase,
  history,
  location: {
    state: { user }
  }
}) => {
  if (!user) {
    toast.error("Wrong user ID!");
    history.push("/");
  }
  return (
    <>
      <Button
        className="mt-2"
        outline
        onClick={history.goBack}
        color="primary"
        size="sm"
      >
        Back
      </Button>
      <div className="flex-grow-1 d-flex flex-column justify-content-center align-items-center">
        <span>
          <BoldSpanWithMargin>Id: </BoldSpanWithMargin> {user.id}
        </span>
        <Formik
          enableReinitialize
          initialValues={{
            role: UserRole.CLIENT,
            address: "",
            cellPhone: "",
            city: "",
            company: "",
            country: "",
            email: "",
            firstName: "",
            homePhone: "",
            lastName: "",
            state: "",
            workPhone: "",
            zip: "",
            disabled: false,
            ...user
          }}
          validationSchema={FormValuesValidationSchema}
          onSubmit={async (
            formValues,
            { setSubmitting, setStatus, setErrors }
          ) => {
            setSubmitting(true);
            try {
              delete formValues["id"];
              formValues["disabled"] = formValues["disabled"] === "true";
              await firebase.user(user.id).update({
                ...formValues
              });
              setSubmitting(false);
              toast.success("Success");
              history.push({
                pathname: `/users/${user.id}/`,
                state: {
                  user: {
                    ...user,
                    ...formValues
                  }
                }
              });
            } catch (err) {
              console.log(err);
            }
          }}
        >
          {({ errors, touched, isSubmitting, submitForm }) => (
            <StyledForm style={{ width: "100%" }}>
              <div className="row">
                <LabelInput
                  type="select"
                  name="role"
                  label="Role"
                  error={errors.category}
                  touched={touched.category}
                >
                  {Object.keys(UserRole)
                    .map(key => (
                      <option key={key} value={UserRole[key]}>
                        {UserRole[key]}
                      </option>
                    ))
                    .slice(1, -1)}
                </LabelInput>
                <LabelInput
                  name="email"
                  className="form-control"
                  placeholder="Email"
                  type="text"
                  error={errors.email}
                  touched={touched.email}
                  label="Email"
                />
                <LabelInput
                  name="firstName"
                  placeholder="First name"
                  type="text"
                  error={errors.firstName}
                  touched={touched.firstName}
                  label="First name"
                />
                <LabelInput
                  name="lastName"
                  className="form-control"
                  placeholder="Last name"
                  type="text"
                  error={errors.lastName}
                  touched={touched.lastName}
                  label="Last name"
                />
                <LabelInput
                  name="country"
                  className="form-control"
                  placeholder="Country"
                  type="text"
                  error={errors.country}
                  touched={touched.country}
                  label="Country"
                />
                <LabelInput
                  name="state"
                  className="form-control"
                  placeholder="State"
                  type="text"
                  error={errors.state}
                  touched={touched.state}
                  label="State"
                />
                <LabelInput
                  name="zip"
                  className="form-control"
                  placeholder="Zip code"
                  type="number"
                  error={errors.zip}
                  touched={touched.zip}
                  label="Zip code"
                />
                <LabelInput
                  name="city"
                  className="form-control"
                  placeholder="City"
                  type="text"
                  error={errors.city}
                  touched={touched.city}
                  label="City"
                />
                <LabelInput
                  name="address"
                  className="form-control"
                  placeholder="Address"
                  type="text"
                  error={errors.address}
                  touched={touched.address}
                  label="Address"
                />
                <LabelInput
                  name="company"
                  className="form-control"
                  placeholder="Company"
                  type="text"
                  error={errors.company}
                  touched={touched.company}
                  label="Company"
                />
                <LabelInput
                  name="cellPhone"
                  className="form-control"
                  placeholder="Cell phone"
                  type="text"
                  error={errors.cellPhone}
                  touched={touched.cellPhone}
                  label="Cell phone"
                />
                <LabelInput
                  name="homePhone"
                  className="form-control"
                  placeholder="Home phone"
                  type="text"
                  error={errors.homePhone}
                  touched={touched.homePhone}
                  label="Home phone"
                />
                <LabelInput
                  name="workPhone"
                  className="form-control"
                  placeholder="Work phone"
                  type="text"
                  error={errors.workPhone}
                  touched={touched.workPhone}
                  label="Work phone"
                />
                <LabelInput
                  name="disabled"
                  className="form-control"
                  type="select"
                  error={errors.disabled}
                  touched={touched.disabled}
                  label="Status"
                >
                  <option value={false}>active</option>
                  <option value={true}>disabled</option>
                </LabelInput>
                <div className="col-sm-12 col-md-6 offset-md-3 my-4">
                  <button
                    type="submit"
                    className="btn btn-primary btn-block button-text brown-background"
                    disabled={isSubmitting}
                    onClick={submitForm}
                  >
                    Update
                  </button>
                </div>
              </div>
            </StyledForm>
          )}
        </Formik>
      </div>
    </>
  );
};

export default compose(withFirebase)(EditFlightForm);
