import React from "react";
import { Button, Col, Row } from "reactstrap";
import { withFirebase } from "../../Firebase";
import { toast } from "react-toastify";
import Spinner from "../../Spinner";
import { compose } from "recompose";
import { Link } from "react-router-dom";
import UserInfo from "./UserInfo";

const User = ({
  match: { params },
  firebase,
  location: {
    state: { user }
  },
  history
}) => {
  if (!user) {
    toast.error("Wrong user ID!");
    history.push("/");
  }

  const deleteUser = async () => {
    try {
      await firebase.user(user.id).update({ disabled: true });
      toast.success("Deleted");
      history.push("/users");
    } catch (err) {
      console.log(err);
      toast.error("Something went wrong:(");
    }
  };

  return user ? (
    <>
      <Button
        className="mt-2"
        outline
        onClick={history.goBack}
        color="primary"
        size="sm"
      >
        Back
      </Button>
      <UserInfo user={user} />
      <Row className="mt-4">
        <Col xs={{ size: 3, offset: 2 }}>
          <Button
            block
            color="primary"
            tag={Link}
            to={{
              pathname: `/users/${user.id}/edit`,
              state: {
                user
              }
            }}
          >
            Edit
          </Button>
        </Col>
        <Col xs={{ size: 3, offset: 2 }}>
          <Button onClick={deleteUser} block color="danger">
            Delete
          </Button>
        </Col>
      </Row>
    </>
  ) : (
    <Spinner />
  );
};

export default compose(withFirebase)(User);
