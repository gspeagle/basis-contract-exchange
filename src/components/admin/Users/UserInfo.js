import React from "react";
import styled from "styled-components";
import { withFirebase } from "../../Firebase";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const userInfo = ({ user }) => (
  <>
    <Grid item sm={12} md={6}>
      <DataContainer>
        <Header>
          <BoldSpanWithMargin>Id</BoldSpanWithMargin>: {user.id}
        </Header>
      </DataContainer>

      <Grid container spacing={5}>
        <Grid item sm={12} md={6}>
          <DataDiv>
            <BoldSpan>Role: </BoldSpan>
            {user.role}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Email: </BoldSpan>
            {user.email}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Status: </BoldSpan>
            {user.disabled ? "disabled" : "active"}
          </DataDiv>
          <DataDiv>
            <BoldSpan>First name: </BoldSpan>
            {user.firstName}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Last name: </BoldSpan>
            {user.lastName}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Country: </BoldSpan>
            {user.country}
          </DataDiv>
          <DataDiv>
            <BoldSpan>State: </BoldSpan>
            {user.state}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Zip code: </BoldSpan>
            {user.zip}
          </DataDiv>
          <DataDiv>
            <BoldSpan>City: </BoldSpan>
            {user.city}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Address: </BoldSpan>
            {user.address}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Company: </BoldSpan>
            {user.company}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Cell phone: </BoldSpan>
            {user.cellPhone}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Work phone: </BoldSpan>
            {user.workPhone}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Home phone: </BoldSpan>
            {user.homePhone}
          </DataDiv>
        </Grid>
      </Grid>
    </Grid>
  </>
);
const Header = styled(Typography).attrs(() => ({
  variant: "h4"
}))`
  margin-bottom: 25px !important;
`;

const DataContainer = styled.div``;
const BoldSpan = styled.span`
  font-weight: 700;
`;

export const BoldSpanWithMargin = styled(BoldSpan)`
  margin-bottom: 24px;
`;

const DataDiv = styled.div`
  text-align: left;
  margin-left: 24px;
  margin-bottom: 16px;
  font-size: 125%;
`;

export default withFirebase(userInfo);
