import React, { useEffect, useState } from "react";
import { withFirebase } from "../../Firebase";
import Users from "./Users";
import { compose } from "recompose";
import { inject } from "mobx-react";

const UsersContainer = ({ firebase }) => {
  const [users, setUsers] = useState(null);
  useEffect(() => {
    return firebase.users().onSnapshot(querySnapshot => {
      const users = querySnapshot.docs.map(doc => ({
        id: doc.id,
        ...doc.data()
      }));
      setUsers(users);
    }, console.error);
  }, []);

  return users && <Users users={users} />;
};

export default compose(
  inject("UserStore"),
  withFirebase
)(UsersContainer);
