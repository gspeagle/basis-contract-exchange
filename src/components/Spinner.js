import React from "react";
import { Spinner as ReactstrapSpinner } from "reactstrap";

const Spinner = props => (
  <ReactstrapSpinner style={{ width: "3rem", height: "3rem" }} color="info" />
);
export default Spinner;
