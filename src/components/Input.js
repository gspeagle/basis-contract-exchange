import React from "react";
import Input from "reactstrap/es/Input";
import { Field } from "formik";
import { BoldSpan, StyledIconSpan } from "../utils/styled";

const LabelInput = ({ error, touched, icon, label, size, name, ...rest }) => (
  <div className={"form-group col-sm-" + (size || 6)}>
    {label && <label>{label}</label>}
    <div className="input-group">
      {icon && (
        <div className="input-group-prepend">
          <StyledIconSpan className="input-group-text">
            <i className={"fas " + icon} />
          </StyledIconSpan>
        </div>
      )}
      <Field
        name={name}
        render={({ field }) => (
          <Input invalid={error && touched} {...rest} {...field} />
        )}
      />

      {error && touched && <div className="invalid-feedback">{error}</div>}
    </div>
  </div>
);

const input = ({ error, touched, icon, name, size, ...rest }) => (
  <div className={"input-group col-sm-" + (size || 12)}>
    {icon && (
      <div className="input-group-prepend">
        <span className="input-group-text">
          <i className={"fas " + icon} />
        </span>
      </div>
    )}
    <Field
      name={name}
      render={({ field }) => (
        <Input invalid={error && touched} {...rest} {...field} />
      )}
    />

    {error && touched && <div className="invalid-feedback">{error}</div>}
  </div>
);

export default input;
export { LabelInput };
