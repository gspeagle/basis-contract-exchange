import React, { useState } from "react";
import { Button, Table } from "reactstrap";
import moment from "moment";
import Modal from "../../components/Modal";
import Spinner from "../../components/Spinner";
import { withFirebase } from "../Firebase";
import { OfferStatus } from "../../constants/OfferStatusEnum";

const offerList = ({ loading, offers, firebase, onAccept }) => {
  const [isAcceptOffer, setIsAcceptOffer] = useState(false);
  const [offer, setOffer] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const openModal = offer => {
    setOffer(offer);
    toggle();
  };
  const showDeclineModal = offer => {
    setIsAcceptOffer(false);
    openModal(offer);
  };
  const showAcceptModal = offer => {
    setIsAcceptOffer(true);
    openModal(offer);
  };
  return (
    <>
      <h4 className="text-center pt-5 pb-3">Offers</h4>
      {loading ? (
        <Spinner />
      ) : (
        <Table hover responsive size="sm">
          <thead>
            <tr>
              <th>Id</th>
              <th>Commodity</th>
              <th>Location</th>
              <th>Type</th>
              <th>Delivery by</th>
              <th>Price</th>
              <th>Units</th>
              <th>Status</th>
              <th>Expires</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {offers.map(data => (
              <tr key={data.id}>
                <th scope="row">{data.id}</th>
                <td>{data.commodityName}</td>
                <td>{data.location}</td>
                <td>{data.offerType}</td>
                <td>{moment(data.deliveryAnd).format("MMM DD h:mm A")}</td>
                <td>{`$${data.bushels}`}</td>
                <td>{data.quantity}</td>
                <td>{data.status}</td>
                <td>{moment(data.expiration).format("MMM DD h:mm A")}</td>

                <td>
                  <Button
                    outline
                    color="success"
                    size="sm"
                    className="mr-2"
                    onClick={() => showAcceptModal(data)}
                  >
                    Accept
                  </Button>
                  <Button
                    outline
                    color="danger"
                    size="sm"
                    className="ml-2"
                    onClick={() => showDeclineModal(data)}
                  >
                    Decline
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      )}

      <Modal
        okAction={async () => {
          if (isAcceptOffer) {
            await firebase
              .offer(offer.id)
              .update({ status: OfferStatus.APPROVED });
            toggle();
          } else {
            await firebase
              .offer(offer.id)
              .update({ status: OfferStatus.CANCELED });
            toggle();
          }
        }}
        isOpen={isOpen}
        toggle={toggle}
        body={`Do you want ${isAcceptOffer ? "accept" : "decline"} this offer`}
        titile={`Offer ${offer.id}`}
      />
    </>
  );
};

export default withFirebase(offerList);
