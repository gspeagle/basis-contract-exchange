import React, { useState } from "react";
import * as yup from "yup";
import { Formik } from "formik";
import { StyledForm } from "../../utils/styled";
import { LabelInput } from "../../components/Input";
import { withFirebase } from "../Firebase";
import { toast } from "react-toastify";
import {
  basicRequiredStringValidationSchema,
  dateFutureValidationSchema,
  numberValidationSchema,
  positiveNumberValidationSchema
} from "../../utils/validations";
import { YEAR_FORMAT } from "../../views/offer/OfferForm";
import moment from "moment";
import { compose } from "recompose";
import { inject } from "mobx-react";
import { categories } from "../../utils/constants";
import Select from "../Select";
import Col from "reactstrap/es/Col";

const FormValuesValidationSchema = yup.object().shape({
  category: basicRequiredStringValidationSchema,
  deliveryDate: dateFutureValidationSchema,
  basis: numberValidationSchema,
});

const NewFlightForm = ({ firebase, history, UserStore }) => {
  const [elevators, setElevators] = useState([]);
  const [selectedElevator, setSelectedElevator] = useState([]);
  const handleChange = async value => {
    if (value.length >= 3) {
      let next = value;
      next =
        next.substr(0, next.length - 1) +
        String.fromCharCode(next.charCodeAt(next.length - 1) + 1);
      const ref = firebase
        .elevators()
        .where("name", ">=", value)
        .where("name", "<", next)
        .orderBy("name")
        .limit(20);
      const snapshot = await ref.get();
      if (snapshot.empty) {
        setElevators([]);
        return;
      }

      const data = [];
      const elevators = [];
      snapshot.forEach(doc => {
        const elevator = doc.data();
        if (!elevators.includes(elevator.name)) {
          elevators.push(elevator.name);
          data.push({ id: doc.id, ...elevator });
        }
      });
      setElevators(data);
    } else {
      setElevators([]);
    }
  };
  return (
    <div className="flex-grow-1 d-flex flex-row justify-content-center align-items-center">
      <Formik
        initialValues={{
          deliveryDate: moment().format(YEAR_FORMAT),
          basis: 0,
          farmer: "",
          bushelsNumber: "",
          departureLocation: ""
        }}
        validationSchema={FormValuesValidationSchema}
        onSubmit={async (values, { setSubmitting, setStatus, setErrors }) => {
          setSubmitting(true);
          try {
            const ref = await firebase.userContracts(UserStore.user.uid).add({
              ...values,
              uid: UserStore.user.uid
            });
            toast.success("Created new contract!");
            history.push(`/contracts/${ref.id}`);
          } catch (err) {
            console.log(err);
            toast.error("Something went wrong:(");
          }
          setSubmitting(false);
        }}
      >
        {({
          errors,
          touched,
          isSubmitting,
          submitForm,
          setFieldValue,
          setFieldTouched,
          values
        }) => (
          <StyledForm style={{ width: "100%" }}>
            <div className="row">
              <Col sm={6}>
                <div className="d-flex flex-column">
                  <span className="mb-2">Category</span>
                  <Select
                    options={Object.keys(categories)
                      .map(key =>
                        [{ label: key, value: key, isDisabled: true }].concat(
                          categories[key].map(category => ({
                            label: category,
                            value: category
                          }))
                        )
                      )
                      .reduce((acc, categories) => acc.concat(categories))}
                    onChange={option => setFieldValue("category", option.value)}
                    onBlur={() => setFieldTouched("category")}
                    error={errors.category}
                    touched={touched.category}
                  />
                </div>
              </Col>
              />
              <Col sm={6}>
                <div className="d-flex flex-column">
                  <span className="mb-2">Elevator</span>
                  <Select
                    options={elevators.map(elevator => ({
                      label: elevator.name,
                      value: elevator.id,
                      address: elevator.address
                    }))}
                    onInputChange={value => {
                      handleChange(value);
                    }}
                    onChange={option => {
                      setSelectedElevator(option.address);
                      setFieldValue("elevator", option.value);
                    }}
                    onBlur={() => setFieldTouched("elevator")}
                    error={errors.elevator}
                    touched={touched.elevator}
                  />
                </div>
              </Col>
              <Col sm={6}>
                <div className="d-flex flex-column">
                  <span className="mb-2">Elevator location</span>
                  {selectedElevator}
                </div>
              </Col>
              <LabelInput
                name="deliveryDate"
                className="form-control"
                placeholder="Delivery date"
                type="date"
                error={errors.deliveryDate}
                touched={touched.deliveryDate}
                label="Delivery date"
              />
              <LabelInput
                name="basis"
                className="form-control"
                placeholder="Basis"
                type="number"
                error={errors.basis}
                touched={touched.basis}
                label="Basis"
              />
              <LabelInput
                name="farmer"
                className="form-control"
                placeholder="Farmer"
                type="text"
                error={errors.farmer}
                touched={touched.farmer}
                label="Farmer"
              />
              <LabelInput
                name="bushelsNumber"
                className="form-control"
                placeholder="Minimum 5000"
                type="number"
                error={errors.bushelsNumber}
                touched={touched.bushelsNumber}
                label="Total number of Bushels"
              />
              <LabelInput
                name="departureLocation"
                className="form-control"
                placeholder="Delivery Location"
                type="text"
                error={errors.departureLocation}
                touched={touched.departureLocation}
                label="Delivery Location"
              />
              <div className="col-sm-12 col-md-6 offset-md-3 my-4">
                <button
                  type="submit"
                  className="btn btn-primary btn-block button-text brown-background"
                  disabled={isSubmitting}
                  onClick={submitForm}
                >
                  Create
                </button>
              </div>
            </div>
          </StyledForm>
        )}
      </Formik>
    </div>
  );
};

export default compose(
  withFirebase,
  inject("UserStore")
)(NewFlightForm);