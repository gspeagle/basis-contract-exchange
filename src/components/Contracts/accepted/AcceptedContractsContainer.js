import React, { useEffect, useState } from "react";
import { withFirebase } from "../../Firebase";
import ContractsView from "./AcceptedContractsView";
import { compose } from "recompose";
import { inject } from "mobx-react";
import { ContractStatus } from "../../../constants/ContractStatusEnum";

const AcceptedContractsContainer = ({ firebase, UserStore }) => {
  const [contracts, setContracts] = useState(null);
  useEffect(() => {
    return firebase
      .userContracts(UserStore.user.uid)
      .where("status", "==", ContractStatus.ACCEPTED)
      .onSnapshot(querySnapshot => {
        const contracts = querySnapshot.docs.map(doc => ({
          id: doc.id,
          ...doc.data()
        }));
        setContracts(contracts);
      }, console.error);
  }, []);
  console.log(UserStore.user.uid);
  return contracts && <ContractsView contracts={contracts} />;
};

export default compose(
  inject("UserStore"),
  withFirebase
)(AcceptedContractsContainer);
