import React from "react";
import Spinner from "../../Spinner";
import Grid from "@material-ui/core/Grid";
import { BoldSpanWithMargin } from "../ContractInfo";
import { BoldSpan } from "../../../utils/styled";
import moment from "moment";
import styled from "styled-components";

const DataDiv = styled(Grid).attrs(() => ({
  item: true,
  sm: 4
}))`
  text-align: left;
  margin-left: 24px;
  margin-bottom: 16px;
  font-size: 125%;
`;
const AcceptedOfferView = ({ offer, client }) => {
  return offer && client ? (
    <Grid item sm={12} md={6}>
      <div>
        <h3>
          <BoldSpanWithMargin>Offer</BoldSpanWithMargin>:
        </h3>
      </div>

      <Grid container spacing={5}>
        <DataDiv>
          <BoldSpan>Price Bushels at: </BoldSpan>
          {offer.bushels}
        </DataDiv>
        <DataDiv>
          <BoldSpan>Quantity: </BoldSpan>
          {offer.quantity}
        </DataDiv>
        <DataDiv>
          <BoldSpan>Contract type: </BoldSpan>
          {offer.contractType}
        </DataDiv>
        <DataDiv>
          <BoldSpan>Delivery Date: </BoldSpan>
          {moment(offer.deliveryBetween).format("h:mm:ss a MMM DD") +
            " - " +
            moment(offer.deliveryAnd).format("h:mm:ss a MMM DD")}
        </DataDiv>
        <DataDiv>
          <BoldSpan>Delivery type: </BoldSpan>
          {offer.deliveryType}
        </DataDiv>
        <DataDiv>
          <BoldSpan>Delivery Location: </BoldSpan>
          {offer.location}
        </DataDiv>
        <DataDiv>
          <BoldSpan>Client: </BoldSpan>
          {`${client.firstName} ${client.lastName}`}
        </DataDiv>
        <DataDiv>
          <BoldSpan>Contact 1: </BoldSpan>
          {offer.contact1}
        </DataDiv>
        <DataDiv>
          <BoldSpan>Contact 2: </BoldSpan>
          {offer.contact2}
        </DataDiv>
        <DataDiv>
          <BoldSpan>Comments: </BoldSpan>
          {offer.comments}
        </DataDiv>
      </Grid>
    </Grid>
  ) : (
    <Spinner />
  );
};

export default AcceptedOfferView;
