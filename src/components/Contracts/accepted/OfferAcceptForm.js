import React from "react";
import styled from "styled-components";
import { Formik } from "formik";
import { withFirebase } from "../../Firebase";
import { compose } from "recompose";
import { inject } from "mobx-react";
import { toast } from "react-toastify";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const OfferAcceptedForm = ({ offer, firebase }) => (
  <Formik
    initialValues={{
      publicAddress: ""
    }}
    onSubmit={async (values, { setSubmitting, setStatus, setErrors }) => {
      setSubmitting(true);
      const { publicAddress } = values;
      await firebase.offer(offer.id).update({ publicAddress });
      toast.success("Contract confirmed!");
      setStatus({ submitted: true });
      setSubmitting(false);
    }}
  >
    {({
      errors,
      touched,
      isSubmitting,
      handleSubmit,
      values,
      status,
      handleChange
    }) => {
      return (status && status.submitted) || offer.publicAddress ? (
        <Grid item sm={12} md={6}>
          You confirmed contract! Public Address:{" "}
          {offer.publicAddress || values.publicAddress}
        </Grid>
      ) : (
        <Grid item sm={12} md={6}>
          <form onSubmit={handleSubmit}>
            <FormInput>
              <TextField
                type="text"
                fullWidth
                label="Public TZ address"
                placeholder="tz1TYhmuZw5gEbhMikiM8eMCK9UVU1GRPS3Q"
                onChange={handleChange}
                inputProps={{ name: "publicAddress" }}
                margin="normal"
                error={!!(touched.publicAddress && errors.publicAddress)}
                value={values.publicAddress}
              />
            </FormInput>
            <Button
              variant="outlined"
              color="primary"
              type="submit"
              disabled={isSubmitting}
            >
              Submit
            </Button>
          </form>
        </Grid>
      );
    }}
  </Formik>
);

const FormInput = styled.div`
  margin-bottom: 16px;
  text-align: left;
`;

export default compose(
  withFirebase,
  inject("UserStore")
)(OfferAcceptedForm);
