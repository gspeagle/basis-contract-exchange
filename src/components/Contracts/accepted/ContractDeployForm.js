import React from "react";
import styled from "styled-components";
import { Formik } from "formik";
import { withFirebase } from "../../Firebase";
import { compose } from "recompose";
import { inject } from "mobx-react";
import { toast } from "react-toastify";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import moment from "moment";

const contractCode = `parameter (or (or (or (unit %confirm_delivery) (unit %deposit)) (unit %send_money_to_elevator)) (bool %test));
storage (pair (pair (pair (pair (pair (pair (pair (pair (pair (int %basis) (int %bushels)) (bool %closed)) (bool %delivery_confirmed)) (timestamp %delivery_date)) (bool %deposited)) (address %elevator)) (address %farmer)) (int %future_price)) (mutez %total_to_pay));
code
  {
    DUP; CDR; SWAP; CAR;
    IF_LEFT
      {
        IF_LEFT
          {
            IF_LEFT
              {
                PAIR; DUP; CDAADR; SENDER; COMPARE; EQ;
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.sender == self.data.farmer"; FAILWITH;
                  };
                DUP; CDAAAAAADR; NOT;
                IF
                  {}
                  {
                    PUSH string "WrongCondition: ~ self.data.delivery_confirmed"; FAILWITH;
                  };
                DUP; CDAAAADR;
                IF
                  {}
                  {
                    PUSH string "WrongCondition: self.data.deposited"; FAILWITH;
                  };
                DUP; CDAAAAAAADR; NOT;
                IF
                  {}
                  {
                    PUSH string "WrongCondition: ~ self.data.closed"; FAILWITH;
                  };
                DUP; CDR; PUSH bool True; SWAP; SET_CAAAAAADR; SWAP; DROP; NIL operation; PAIR;
              }
              {
                PAIR; DUP; CDAAAAAAADR; NOT;
                IF
                  {}
                  {
                    PUSH string "WrongCondition: ~ self.data.closed"; FAILWITH;
                  };
                DUP; CDAADR; SENDER; COMPARE; EQ;
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.sender == self.data.farmer"; FAILWITH;
                  };
                AMOUNT; DUUP; CDDR; COMPARE; EQ;
                IF
                  {}
                  {
                    PUSH string "WrongCondition: self.data.total_to_pay == sp.amount"; FAILWITH;
                  };
                DUP; CDR; PUSH bool True; SWAP; SET_CAAAADR; SWAP; DROP; NIL operation; PAIR;
              };
          }
          {
            PAIR; NOW; DUUP; CDAAAAADR; COMPARE; LT;
            IF
              {}
              {
                PUSH string "WrongCondition: self.data.delivery_date < sp.currentTime"; FAILWITH;
              };
            DUP; CDAAAAAADR;
            IF
              {}
              {
                PUSH string "WrongCondition: self.data.delivery_confirmed"; FAILWITH;
              };
            DUP; CDAAAADR;
            IF
              {}
              {
                PUSH string "WrongCondition: self.data.deposited"; FAILWITH;
              };
            DUP; CDAAADR; SENDER; COMPARE; EQ;
            IF
              {}
              {
                PUSH string "WrongCondition: sp.sender == self.data.elevator"; FAILWITH;
              };
            DUP; CDAAAAAAADR; NOT;
            IF
              {}
              {
                PUSH string "WrongCondition: ~ self.data.closed"; FAILWITH;
              };
            DUP; CDR; PUSH bool True; SWAP; SET_CAAAAAAADR; SWAP; DROP; NIL operation; PAIR;
          };
      }
      {
        PAIR; DUP; CDR; DUUP; CAR; SWAP; SET_CAAAAAADR; SWAP; DROP; NIL operation; PAIR;
      };
  }`;

const ContractDeployForm = ({
  offer,
  contract,
  firebase,
  UserStore,
  setContract
}) => (
  <Formik
    initialValues={{
      mnemonic:
        "monitor knock child because canal section purpose tunnel erase kiss slight flag biology tunnel hint",
      password: "4xnpZ0ueJi",
      email: "fvturwmu.bmnsvwav@tezos.example.org",
      publicAddress: "tz1hb8XRoHAV5BkjFZ7G7yCaRY9XVCa8fhmd"
    }}
    onSubmit={async (values, { setSubmitting, setStatus, setErrors }) => {
      setSubmitting(true);
      const { mnemonic, email, password, publicAddress } = values;
      const keystore = await conseiljs.TezosWalletUtil.unlockFundraiserIdentity(
        mnemonic,
        email,
        password,
        publicAddress
      );
      const bushels = offer.quantity;
      const future_price = Math.round(offer.bushels);
      const basis = contract.basis;
      const total_to_pay = 10 ** 5 * (bushels * (future_price - basis));
      const elevator = keystore.publicKeyHash;
      const farmer = offer.publicAddress;
      const delivery_date = moment(offer.deliveryBetween)
        .unix()
        .toString();
      const storage = `(Pair (Pair (Pair (Pair (Pair (Pair (Pair (Pair (Pair ${basis} ${bushels}) False) False) "${delivery_date}") False) "${elevator}") "${farmer}") ${future_price}) ${total_to_pay})`;
      const tezosNode = "http://alphanet-node.tzscan.io:80";

      const result = await conseiljs.TezosNodeWriter.sendContractOriginationOperation(
        tezosNode,
        keystore,
        0,
        undefined,
        false,
        true,
        100000,
        "",
        4000,
        100000,
        contractCode,
        storage,
        conseiljs.TezosParameterFormat.Michelson
      );
      if (
        result.results.contents[0].metadata.operation_result.status ===
        "applied"
      ) {
        const deployedContract =
          result.results.contents[0].metadata.operation_result
            .originated_contracts[0];
        console.log(deployedContract);
        await firebase
          .userContract(UserStore.user.uid, contract.id)
          .update({ deployedContract });
        setContract({ ...contract, deployedContract });
        toast.success("Contract deployed!");
      } else {
        console.log(result);
        toast.error("Deployment error!");
      }
      // await firebase.offer(offer.id).update({ publicAddress });
      setStatus({ submitted: true });
      setSubmitting(false);
    }}
  >
    {({
      errors,
      touched,
      isSubmitting,
      handleSubmit,
      values,
      status,
      handleChange
    }) => {
      return status && status.submitted ? (
        <Grid item sm={12} md={6}>
          You confirmed contract! Public Address:{" "}
          {offer.publicAddress || values.publicAddress}
        </Grid>
      ) : (
        <Grid item sm={12} md={6}>
          <form onSubmit={handleSubmit}>
            <FormInput>
              <TextField
                type="text"
                fullWidth
                label="Mnemonic"
                placeholder="drop crumble memory during coyote wrist dynamic donkey peanut palace buffalo mammal super clog fish"
                onChange={handleChange}
                inputProps={{ name: "mnemonic" }}
                margin="normal"
                error={!!(touched.mnemonic && errors.mnemonic)}
                value={values.mnemonic}
              />
            </FormInput>
            <FormInput>
              <TextField
                type="text"
                fullWidth
                label="Password for mnemonic"
                placeholder="passphrase"
                onChange={handleChange}
                inputProps={{ name: "password" }}
                margin="normal"
                error={!!(touched.password && errors.password)}
                value={values.password}
              />
            </FormInput>
            <FormInput>
              <TextField
                type="email"
                fullWidth
                label="Email"
                placeholder="awesome@email.com"
                onChange={handleChange}
                inputProps={{ name: "email" }}
                margin="normal"
                error={!!(touched.email && errors.email)}
                value={values.email}
              />
            </FormInput>
            <FormInput>
              <TextField
                type="text"
                fullWidth
                label="Public TZ address"
                placeholder="tz1TYhmuZw5gEbhMikiM8eMCK9UVU1GRPS3Q"
                onChange={handleChange}
                inputProps={{ name: "publicAddress" }}
                margin="normal"
                error={!!(touched.publicAddress && errors.publicAddress)}
                value={values.publicAddress}
              />
            </FormInput>
            <Button
              variant="outlined"
              color="primary"
              type="submit"
              disabled={isSubmitting}
            >
              Deploy smart contract
            </Button>
          </form>
        </Grid>
      );
    }}
  </Formik>
);

const FormInput = styled.div`
  margin-bottom: 16px;
  text-align: left;
`;

export default compose(
  withFirebase,
  inject("UserStore")
)(ContractDeployForm);
