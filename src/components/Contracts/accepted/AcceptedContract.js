import React, { useEffect, useState } from "react";
import { Button, Col, Row } from "reactstrap";
import { withFirebase } from "../../Firebase";
import { toast } from "react-toastify";
import { compose } from "recompose";
import { inject } from "mobx-react";
import ContractInfo from "../ContractInfo";
import AcceptedOfferView from "./AcceptedOfferView";
import Grid from "@material-ui/core/Grid";
import styled from "styled-components";
import Spinner from "../../Spinner";
import Typography from "@material-ui/core/Typography";
import ContractDeployForm from "./ContractDeployForm";

const BackButton = styled(Button)`
  position: fixed;
  margin-top: 24px;
  margin-left: -100px;
`;

const AcceptedContract = ({
  match: { params },
  firebase,
  history,
  UserStore
}) => {
  const [contract, setContract] = useState(null);
  const [offer, setOffer] = useState(null);
  const [client, setClient] = useState(null);
  useEffect(() => {
    async function fetchContract() {
      try {
        const ref = await firebase
          .userContract(UserStore.user.uid, params.contractId)
          .get();
        if (ref.exists) {
          setContract({ ...ref.data(), id: ref.id });
        } else {
          toast.error("Wrong contract ID!");
          history.push("/");
        }
      } catch (err) {
        console.log(err);
        toast.error("Something went wrong:(");
      }
    }
    fetchContract();
  }, []);
  useEffect(() => {
    async function fetchOffer() {
      try {
        const ref = await firebase.offer(contract.approvedOffer).get();
        if (ref.exists) {
          setOffer({ ...ref.data(), id: ref.id });
        } else {
          toast.error("Wrong offer ID!");
          history.push("/");
        }
      } catch (err) {
        console.log(err);
        toast.error("Something went wrong:(");
      }
    }
    if (contract) {
      fetchOffer();
    }
  }, [contract]);

  useEffect(() => {
    async function fetchClient() {
      try {
        const ref = await firebase.user(offer.uid).get();
        if (ref.exists) {
          setClient(ref.data());
        } else {
          toast.error("Wrong offer ID!");
          history.push("/");
        }
      } catch (err) {
        console.log(err);
        toast.error("Something went wrong:(");
      }
    }
    if (offer) {
      fetchClient();
    }
  }, [offer]);
  return contract && offer ? (
    <>
      <BackButton outline onClick={history.goBack} color="primary" size="sm">
        Back
      </BackButton>
      <Grid container spacing={1}>
        <ContractInfo contract={contract} />
        <AcceptedOfferView offer={offer} client={client} />
      </Grid>
      <Typography variant="h4" gutterBottom>
        Status:
        {!offer.publicAddress
          ? " waiting for client confirmation"
          : contract.deployedContract
          ? ` contract deployed, ${contract.deployedContract}`
          : ` client confirmed, public address ${offer.publicAddress}`}
      </Typography>
      {offer.publicAddress && !contract.deployedContract && (
        <ContractDeployForm
          contract={contract}
          offer={offer}
          setContract={setContract}
        />
      )}
    </>
  ) : (
    <Spinner />
  );
};

export default compose(
  inject("UserStore"),
  withFirebase
)(AcceptedContract);
