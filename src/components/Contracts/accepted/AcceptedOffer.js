import React, { useEffect, useState } from "react";
import { Button } from "reactstrap";
import { withFirebase } from "../../Firebase";
import { toast } from "react-toastify";
import { compose } from "recompose";
import AcceptedOfferView from "./AcceptedOfferView";
import styled from "styled-components";
import Grid from "@material-ui/core/Grid";
import OfferAcceptedForm from "./OfferAcceptForm";
import Spinner from "../../Spinner";

const BackButton = styled(Button)`
  position: fixed;
  margin-top: 24px;
  margin-left: -100px;
`;

const AcceptedOffer = ({ match: { params }, firebase, history }) => {
  const [offer, setOffer] = useState(null);
  const [client, setClient] = useState(null);
  useEffect(() => {
    async function fetchOffer() {
      try {
        const ref = await firebase.offer(params.offerId).get();
        if (ref.exists) {
          setOffer({ ...ref.data(), id: ref.id });
        } else {
          toast.error("Wrong offer ID!");
          history.push("/");
        }
      } catch (err) {
        console.log(err);
        toast.error("Something went wrong:(");
      }
    }
    fetchOffer();
  }, []);
  useEffect(() => {
    async function fetchClient() {
      try {
        const ref = await firebase.user(offer.uid).get();
        if (ref.exists) {
          setClient(ref.data());
        } else {
          toast.error("Wrong offer ID!");
          history.push("/");
        }
      } catch (err) {
        console.log(err);
        toast.error("Something went wrong:(");
      }
    }
    if (offer) {
      fetchClient();
    }
  }, [offer]);
  return offer && client ? (
    <>
      <BackButton outline onClick={history.goBack} color="primary" size="sm">
        Back
      </BackButton>
      <Grid container spacing={1}>
        <AcceptedOfferView offer={offer} client={client} />
        <OfferAcceptedForm offer={offer} />
      </Grid>
    </>
  ) : (
    <Spinner />
  );
};

export default compose(withFirebase)(AcceptedOffer);
