import React, { useEffect, useState } from "react";
import * as yup from "yup";
import { Formik } from "formik";
import { StyledForm } from "../../utils/styled";
import { LabelInput } from "../../components/Input";
import { withFirebase } from "../Firebase";
import { toast } from "react-toastify";
import Spinner from "../Spinner";
import {
  basicRequiredStringValidationSchema,
  dateFutureValidationSchema,
  numberValidationSchema,
  positiveNumberValidationSchema
} from "../../utils/validations";
import { inject } from "mobx-react";
import { compose } from "recompose";
import { categories } from "../../utils/constants";

const FormValuesValidationSchema = yup.object().shape({
  category: basicRequiredStringValidationSchema,
  deliveryDate: dateFutureValidationSchema,
  basis: numberValidationSchema,
});

const EditFlightForm = ({
  match: { params },
  firebase,
  history,
  UserStore
}) => {
  const [values, setValues] = useState(null);

  useEffect(() => {
    async function fetchContract() {
      try {
        const ref = await firebase
          .userContract(UserStore.user.uid, params.contractId)
          .get();
        if (ref.exists) {
          setValues(ref.data());
        } else {
          toast.error("Wrong contract ID!");
          history.push("/");
        }
      } catch (err) {
        console.log(err);
        toast.error("Something went wrong:(");
      }
    }
    fetchContract();
  }, []);
  return values  (
    <div className="flex-grow-1 d-flex flex-row justify-content-center align-items-center">
      <Formik
        enableReinitialize
        initialValues={{
          deliveryDate: "",
          basis: 0,
          farmer: "",
          bushelsNumber: "",
          departureLocation: "",
          ...values
        }}
        validationSchema={FormValuesValidationSchema}
        onSubmit={async (
          formValues,
          { setSubmitting, setStatus, setErrors }
        ) => {
          setSubmitting(true);
          try {
            await firebase
              .userContract(UserStore.user.uid, params.contractId)
              .update({
                ...formValues
              });
            setSubmitting(false);
            toast.success("Success");
            history.push(`/contracts/${params.contractId}`);
          } catch (err) {
            console.log(err);
          }
        }}
      >
        {({ errors, touched, isSubmitting, submitForm }) => (
          <StyledForm style={{ width: "100%" }}>
            <div className="row">
              <LabelInput
                type="select"
                name="category"
                label="Category"
                error={errors.category}
                touched={touched.category}
              />
                {categories.map(category => (
                  <option key={category} value={category}>
                    {category}
                  </option>
                ))}
                {Object.keys(categories)
                  .map(key =>
                    [
                      <option key={key} value={key} disabled>
                        {key}
                      </option>
                    ].concat(
                      categories[key].map(category => (
                        <option key={category} value={category}>
                          {category}
                        </option>
                      ))
                    )
                  )
                  .reduce((acc, categories) => acc.concat(categories))}
              /> 
              <LabelInput
                name="deliveryDate"
                className="form-control"
                placeholder="Delivery date"
                type="date"
                error={errors.deliveryDate}
                touched={touched.deliveryDate}
                label="Delivery date"
              />
              <LabelInput
                name="basis"
                className="form-control"
                placeholder="Basis"
                type="number"
                error={errors.basis}
                touched={touched.basis}
                label="Basis"
              />
              <LabelInput
                name="farmer"
                className="form-control"
                placeholder="Farmer"
                type="text"
                error={errors.farmer}
                touched={touched.farmer}
                label="Farmer"
              />
              <LabelInput
                name="bushelsNumber"
                className="form-control"
                placeholder="Minimum of 5000"
                type="number"
                error={errors.bushelsNumber}
                touched={touched.bushelsNumber}
                label="Total number of Bushels"
              />
              <LabelInput
                name="departureLocation"
                className="form-control"
                placeholder="Delivery Location"
                type="text"
                error={errors.departureLocation}
                touched={touched.departureLocation}
                label="Delivery Location"
              />  
              <div className="col-sm-12 col-md-6 offset-md-3 my-4">
                <button
                  type="submit"
                  className="btn btn-primary btn-block button-text brown-background"
                  disabled={isSubmitting}
                  onClick={submitForm}
                >
                  Update
                </button>
              </div>
            </div>
          </StyledForm>
        )}
      </Formik>
    </div>
  ) ; (
    <Spinner />
   );
  };

export default compose(
  inject("UserStore"),
  withFirebase
)(EditFlightForm);