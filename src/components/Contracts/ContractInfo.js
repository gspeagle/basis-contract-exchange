import React, { useEffect, useState } from "react";
import moment from "moment";
import styled from "styled-components";
import { withFirebase } from "../Firebase";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const contractInfo = ({ contract, firebase }) => {
  const [last, setLast] = useState(null);
  useEffect(() =>
    firebase
      .lastSoldCorn()
      .onSnapshot(doc => setLast(doc.data().price), console.error)
  );
  return (
<<<<<<< HEAD
    <Row>
      <h4 className="col-md-6 mt-3 display-4">
        <BoldSpan>Category: </BoldSpan>
        {contract.category}
      </h4>
      <h4 className="col-md-6 mt-3 display-4">
        <BoldSpan>Name: </BoldSpan>
        {contract.name}
      </h4>

      <p className="col-md-3 my-2 text-center">
        <BoldSpan>Commodity name: </BoldSpan>
        {contract.commodityName}
      </p>
      <p className="col-md-3 my-2 text-center">
        <BoldSpan>Basis month: </BoldSpan>
        {contract.basisMonth}
      </p>
      <p className="col-md-3 my-2 text-center">
        <BoldSpan>Basis: </BoldSpan>
        {contract.basis}
      </p>
      <p className="col-md-3 my-2 text-center">
        <BoldSpan>Cash price: </BoldSpan>
        {contract.cashPrice}
      </p>
      <div className="col-md-6 d-md-flex flex-column justify-content-between">
        <h4>
          <BoldSpan>Basis month: </BoldSpan>
          {contract.basisMonth}
        </h4>
        <h4>
          <BoldSpan>Location: </BoldSpan>
          {contract.location}
        </h4>
        <h4>
          <BoldSpan>Delivery date: </BoldSpan>
          {moment(contract.deliveryDate).format("MMM DD YYYY")}
        </h4>
        <h4>
          <BoldSpan>Last Update: </BoldSpan>
          {moment.unix(contract.updatedAt).format("h:mm:ss a MMM DD")}
        </h4>
        <h4>
          <BoldSpan>Farmer: </BoldSpan>
          {contract.farmer}
        </h4>
        <h4>
          <BoldSpan>Basis Price per Bushel: </BoldSpan>
          {contract.bushels}
        </h4>
        <h4>
          <BoldSpan>Basis Price Per Bushel For 500/Bu of Grain: </BoldSpan>
          {contract.bushels500}
        </h4>
        <h4>
          <BoldSpan>The number of Bushels: </BoldSpan>
          {contract.bushelsNumber}
        </h4>
        <h4>
          <BoldSpan>Departure location: </BoldSpan>
          {contract.departureLocation}
        </h4>
      </div>
      <div className="col-md-6 text-lg-right">
        <h4>
          <BoldSpan>Open: </BoldSpan>
          {contract.open}
        </h4>
        <h4>
          <BoldSpan>Prev: </BoldSpan>
          {contract.prev}
        </h4>
        <h4>
          <BoldSpan>Chg: </BoldSpan>
          {contract.chg}
        </h4>
        <h4>
          <BoldSpan>Last: </BoldSpan>
          {last || "Loading"}
        </h4>
        <h4>
          <BoldSpan>High :</BoldSpan>
          {contract.high}
        </h4>
        <h4>
          <BoldSpan>Low: </BoldSpan>
          {contract.low}
        </h4>
      </div>
    </Row>
=======
    <Grid item sm={12} md={6}>
      <DataContainer>
        <Header>
          <BoldSpanWithMargin>Category</BoldSpanWithMargin>: {contract.category}
        </Header>

        <Header>
          <BoldSpanWithMargin>Name </BoldSpanWithMargin>: {contract.name}
        </Header>
      </DataContainer>

      <Grid container spacing={1}>
        <Grid16pxMargin>
          Commodity name: {contract.commodityName}
        </Grid16pxMargin>
        <Grid16pxMargin>Basis Month: {contract.basisMonth}</Grid16pxMargin>
        <Grid16pxMargin>Basis: {contract.basis}</Grid16pxMargin>
        <Grid16pxMargin>Cash Price: {contract.cashPrice}</Grid16pxMargin>
      </Grid>

      <Grid container spacing={5}>
        <Grid item sm={12} md={6}>
          <DataDiv>
            {" "}
            <BoldSpan>Basis Month: </BoldSpan>
            {contract.basis}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Location: </BoldSpan>
            {contract.location}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Delivery Date: </BoldSpan>
            {moment(contract.deliveryDate).format("MMM DD YYYY")}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Last Update: </BoldSpan>{" "}
            {moment.unix(contract.updatedAt).format("h:mm:ss a MMM DD")}{" "}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Farmer: {contract.farmer}</BoldSpan>
          </DataDiv>
          <DataDiv>
            <BoldSpan>Basis price per Bushell: </BoldSpan>
            {contract.bushels}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Basis price per Bushell For 500/Bu of Grain: </BoldSpan>{" "}
            {contract.bushels500}
          </DataDiv>
          <DataDiv>
            <BoldSpan>The number of Bushels: </BoldSpan>
            {contract.bushelsNumber}
          </DataDiv>
          <DataDiv>
            {" "}
            <BoldSpan>Delivery Location: </BoldSpan>
            {contract.departureLocation}
          </DataDiv>
        </Grid>

        <Grid item sm={12} md={6}>
          <DataDiv>
            <BoldSpan>Open: </BoldSpan>
            {contract.open}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Prev: </BoldSpan>
            {contract.prev}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Chg: </BoldSpan>
            {contract.chg}{" "}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Last: </BoldSpan>
            {contract.last}
          </DataDiv>
          <DataDiv>
            <BoldSpan>High: </BoldSpan>
            {contract.high}{" "}
          </DataDiv>
          <DataDiv>
            <BoldSpan>Low: </BoldSpan>
            {contract.low}
          </DataDiv>
        </Grid>
      </Grid>
    </Grid>
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d
  );
};

const Header = styled(Typography).attrs(() => ({
  variant: "h3"
}))`
  margin-bottom: 25px !important;
`;

export const Grid16pxMargin = styled(Grid).attrs(props => ({
  item: true,
  md: 3,
  lg: 3
}))`
  margin-bottom: 16px !important;
`;

const DataContainer = styled.div``;
export const BoldSpan = styled.span`
  font-weight: 700;
`;

export const BoldSpanWithMargin = styled(BoldSpan)`
  margin-bottom: 24px;
`;

export const DataDiv = styled.div`
  text-align: left;
  margin-left: 24px;
  margin-bottom: 16px;
  font-size: 125%;
`;

export default withFirebase(contractInfo);
