import React, { useEffect, useState } from "react";
import { Button, Col, Row } from "reactstrap";
import { withFirebase } from "../Firebase";
import { toast } from "react-toastify";
import ContractInfo from "./ContractInfo";
import Spinner from "../Spinner";
import { compose } from "recompose";
import { inject } from "mobx-react";
import OfferList from "./OfferList";
import { OfferStatus } from "../../constants/OfferStatusEnum";

const Contract = ({ match: { params }, firebase, history, UserStore }) => {
  const [contract, setContract] = useState(null);
  const [offers, setOffers] = useState(null);

  useEffect(() => {
    async function fetchContract() {
      try {
        const ref = await firebase
          .userContract(UserStore.user.uid, params.contractId)
          .get();
        if (ref.exists) {
          setContract(ref.data());
        } else {
          toast.error("Wrong contract ID!");
          history.push("/");
        }
      } catch (err) {
        console.log(err);
        toast.error("Something went wrong:(");
      }
    }

    fetchContract();
  }, []);
  useEffect(
    () =>
      firebase.db
        .collection(`offers`)
        .where("contractId", "==", params.contractId)
        .where("status", "==", OfferStatus.OPEN)
        .onSnapshot(querySnapshot => {
          const data = [];
          querySnapshot.forEach(offer =>
            data.push({
              ...offer.data(),
              id: offer.id
            })
          );
          setOffers(data);
        }),

    []
  );
  const deleteContract = async () => {
    try {
      await firebase
        .userContract(UserStore.user.uid, params.contractId)
        .delete();
      toast.success("Deleted");
      history.push("/contracts");
    } catch (err) {
      console.log(err);
      toast.error("Something went wrong:(");
    }
  };

  return contract ? (
    <>
      <Button
        className="mt-2"
        outline
        onClick={history.goBack}
        color="primary"
        size="sm"
      >
        Back
      </Button>

      <ContractInfo contract={contract} />
      <Row className="mt-4">
        <Col xs={{ size: 3, offset: 2 }}>
          <Button
            onClick={() => history.push(`${params.contractId}/edit`)}
            block
            color="primary"
          >
            Edit
          </Button>
        </Col>
        <Col xs={{ size: 3, offset: 2 }}>
          <Button onClick={deleteContract} block color="danger">
            Delete
          </Button>
        </Col>
      </Row>

      <OfferList loading={!offers} offers={offers} />
    </>
  ) : (
    <Spinner />
  );
};

export default compose(
  inject("UserStore"),
  withFirebase
)(Contract);
