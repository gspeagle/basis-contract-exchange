import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { Button, Col, Form, FormGroup, Input, Label, Row } from "reactstrap";
import { withFirebase } from "../Firebase";
import ContractsView from "./ContractsView";
import { compose } from "recompose";
import { inject } from "mobx-react";
import { categories } from "../../utils/constants";
import { StyledCol } from "../../utils/styled";
<<<<<<< HEAD
=======
import { ContractStatus } from "../../constants/ContractStatusEnum";
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d

const AddNewButton = styled.div`
  height: 3rem;
  width: 3rem;
  position: fixed;
  bottom: 70px;
  right: 20px;
  cursor: pointer;
  align-items: center;
  border-radius: 50%;
  display: flex;
  background-color: #ccb69a;
  color: #fff;
  font-weight: 300;
  font-size: 1.5rem;
  justify-content: center;
`;

const StyledInput = styled(Input)`
  margin-top: 16px;
  font-size: 36px !important;
`;
const StyledLabel = styled(Label)`
  margin-right: 16px;
`;
const StyledSpan = styled.span`
  margin-right: 8px;
`;

const buttonRender = isManagement => id =>
  isManagement ? (
    <Button
      tag={Link}
      to={`/contracts/${id}`}
      outline
      color="primary"
      size="sm"
    >
      More info
    </Button>
  ) : (
    <Button tag={Link} to={`/offer/${id}`} outline color="primary" size="sm">
      Make Offer
    </Button>
  );

const ContractsContainer = ({ history, firebase, isManagement, UserStore }) => {
  const [contracts, setContracts] = useState(null);
  const [location, setLocation] = useState("CA");
<<<<<<< HEAD
  const [category, setCategory] = useState(categories[0]);
=======
  const [category, setCategory] = useState(categories.Grains[0]);
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d
  const handleLocation = e => setLocation(e.target.value);
  const handleCategoryChange = e => setCategory(e.target.value);
  useEffect(() => {
    const ref = isManagement
      ? firebase.userContracts(UserStore.user.uid)
      : firebase.contracts();
    return ref.where("category", "==", category).onSnapshot(querySnapshot => {
<<<<<<< HEAD
      const contracts = { open: [], CLOSED: [] };
=======
      const contracts = {};
      Object.keys(ContractStatus).forEach(
        key => (contracts[ContractStatus[key]] = [])
      );
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d
      querySnapshot.docs.forEach(doc => {
        const contract = { id: doc.id, ...doc.data() };
        contracts[contract.status].push(contract);
      });
      setContracts(contracts);
    }, console.error);
  }, [isManagement, category]);

  return (
    <>
      <Row>
        <Col md={8} xs={12}>
          <StyledInput
            id="location"
            type="select"
            className="w-auto"
            value={category}
            onChange={handleCategoryChange}
          >
<<<<<<< HEAD
            {categories.map(category => (
              <option key={category} value={category}>
                {category}
              </option>
            ))}
=======
            {Object.keys(categories)
              .map(key =>
                [
                  <option key={key} value={key} disabled>
                    {key}
                  </option>
                ].concat(
                  categories[key].map(category => (
                    <option key={category} value={category}>
                      {category}
                    </option>
                  ))
                )
              )
              .reduce((acc, categories) => acc.concat(categories))}
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d
          </StyledInput>
        </Col>
        <StyledCol md={4} xs={12}>
          <Form inline>
            <FormGroup>
              <StyledLabel for="location">Location:</StyledLabel>
              <Input
                id="location"
                type="select"
                value={location}
                onChange={handleLocation}
              >
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
              </Input>
            </FormGroup>
          </Form>
        </StyledCol>
        <StyledCol sm={{ size: 12 }} md={{ size: 4, offset: 8 }}>
          <Form inline>
            <FormGroup>
              <StyledLabel className="font-weight-bold" for="location">
                LDP Info:
              </StyledLabel>
              <Input id="location" type="select">
                <option value="SD">SD</option>
              </Input>
              <Input id="location" type="select">
                <option value="Codington">Codington</option>
              </Input>
            </FormGroup>
          </Form>
        </StyledCol>
        <StyledCol sm={{ size: 12 }} md={{ size: 4, offset: 8 }}>
          <span className="font-weight-bold">PCP: </span>
          <StyledSpan>$ 1.23</StyledSpan>
          <span className="font-weight-bold">LDP: </span>
          <StyledSpan>$ -1.23</StyledSpan>
          <span className="font-weight-bold">Date: </span>
          <StyledSpan>4/4</StyledSpan>
        </StyledCol>
      </Row>

      {contracts && (
        <ContractsView
          contracts={contracts}
          actionButton={buttonRender(isManagement)}
        />
      )}
      {isManagement && (
        <AddNewButton onClick={() => history.push("/contracts/new")}>
          +
        </AddNewButton>
      )}
    </>
  );
};

export default compose(
  inject("UserStore"),
  withFirebase
)(ContractsContainer);
