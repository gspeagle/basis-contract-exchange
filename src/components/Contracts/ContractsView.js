import React from "react";
import { Row, Table } from "reactstrap";
import withAuthorization from "../Session/WithAuthorization";
import { compose } from "recompose";
import moment from "moment";
import styled from "styled-components";
import ContractsChart from "../charts/ContractsChart";
import { StyledCol } from "../../utils/styled";

const StyledTable = styled(Table)`
  margin-top: 32px;
`;

const cashBids = ({ contracts, actionButton }) => {
  return (
    <>
      <Row>
        <StyledCol sm={12}>
          <ContractsChart contracts={contracts.CLOSED} />
        </StyledCol>
      </Row>
      <StyledTable hover responsive size="sm">
        <thead>
          <tr>
            <th>Delivery Date</th>
            <th>Basis Month</th>
            <th>Basis</th>
            <th>Cash price</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {contracts.open.map(data => (
            <tr key={data.id}>
              <th scope="row">
                {moment(data.deliveryDate).format("MMM DD, YYYY")}
              </th>
              <td>{data.basisMonth}</td>
              <td>{data.basis}</td>
              <td>{data.cashPrice}</td>
              <td>{actionButton(data.id)}</td>
            </tr>
          ))}
        </tbody>
      </StyledTable>
    </>
  );
};

export default compose(withAuthorization)(cashBids);
