import React from "react";
import ReactApexChart from "react-apexcharts";
import { withFirebase } from "../Firebase";

const ContractsChart = ({ contracts }) => {
  const series = contracts
    .map(contract => ({
      x: new Date(contract.closedAt.seconds * 1000),
      y: [contract.open, contract.high, contract.low, contract.price]
    }))
    .sort((a, b) => {
      return a.x > b.x ? 1 : -1;
    });
  const lastSeries =
    series.length > 0
      ? series[series.length - 1].x.getTime()
      : new Date().getTime();
  const twoMinutesInMilliSeconds = 2 * 60 * 1000;
  return (
    <ReactApexChart
      height={500}
      options={{
        xaxis: {
          type: "datetime",
          min: lastSeries - twoMinutesInMilliSeconds,
          max: lastSeries + twoMinutesInMilliSeconds / 4
        },
        chart: {
          id: "candles"
        }
      }}
      type={"candlestick"}
      series={[
        {
          data: series
        }
      ]}
    />
  );
};

export default withFirebase(ContractsChart);
