import React from "react";
import ReactSelect from "react-select";
import styled from "styled-components";

const styles = error => ({
  option: (provided, state) => ({
    ...provided,
    fontWeight: "bold"
  }),
  container: (provided, state) => ({
    ...provided,
    width: "100%",
    fontFamily: " Helvetica, Arial, sans-serif",
    fontWeight: "bold !important",
    fontSize: "14px !important",
    height: "38px !important"
  }),
  control: (provided, state) => ({
    ...provided,
    "&:hover": {
      borderColor: error ? "rgb(220, 53, 69)" : "#ced4da"
    },
    boxShadow: "none",
    borderColor: error ? "rgb(220, 53, 69)" : "#ced4da",
    width: "100%",
    fontFamily: " Helvetica, Arial, sans-serif",
    fontWeight: "bold !important",
    fontSize: "14px !important"
  }),
  input: (provided, state) => ({
    ...provided,
    fontWeight: "bold"
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = "opacity 300ms";

    return {
      ...provided,
      opacity,
      transition,
      fontWeight: "bold"
    };
  }
});

const ValidationError = styled.div`
  margin-top: 4px
  font-size: 12.8px;
`;

const Select = ({ options, error, touched, ...rest }) => {
  return (
    <>
      <ReactSelect
        styles={styles(error && touched)}
        options={options}
        {...rest}
      />
      {error && touched && (
        <ValidationError className="text-danger">{error}</ValidationError>
      )}
    </>
  );
};

export default Select;
