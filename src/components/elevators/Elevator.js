import React, { useState, useEffect } from "react";
<<<<<<< HEAD
import { Link } from "react-router-dom";
import { Row, Col, Button } from "reactstrap";
=======
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Button, Col, Row } from "reactstrap";
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d
import { toast } from "react-toastify";
import { BoldSpan } from "../../utils/styled";
import { withFirebase } from "../Firebase";
import Spinner from "../Spinner";
<<<<<<< HEAD

const Elevator = ({ firebase, match: { params }, history }) => {
  const [elevator, setElevator] = useState();
=======
import { Can } from "../ability";

const Elevator = ({ firebase, match: { params }, history }) => {
  const [elevator, setElevator] = useState();
  const [uploadedPhotos, setuploadedPhotos] = useState([]);
  const [photos, setPhotos] = useState([]);

  const handlePhotoInput = ({ target: { files } }) => {
    setuploadedPhotos([...files]);
  };

  const uploadFiles = () => {
    let uploadedFilesCount = 0;
    uploadedPhotos.forEach(async photo => {
      try {
        const elevatorImagesRef = firebase.storage.child(
          `elevators/${params.elevatorId}/${photo.name}`
        );
        await elevatorImagesRef.put(photo);
      } catch (err) {
        toast.error(err.message);
      }
      uploadedFilesCount++;
      if (uploadedFilesCount === uploadedPhotos.length) {
        setuploadedPhotos();
        toast.success("All files successfull uploaded");
      }
    });
  };

  async function fetchPhotos() {
    try {
      const storageRef = firebase.storage;
      const imagesRef = storageRef.child(`elevators/${params.elevatorId}`);
      const res = await imagesRef.listAll();
      const promises = res.items.map(async photo => {
        const url = await photo.getDownloadURL();
        return { url, fullPath: photo.fullPath };
      });
      const urls = await Promise.all(promises);
      setPhotos([...urls]);
    } catch (err) {
      console.log(err);
      toast.error("Something went wrong:(");
    }
  }

  useEffect(() => {
    uploadedPhotos.length === 0 && fetchPhotos();
  }, [uploadedPhotos]);
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d

  useEffect(() => {
    async function fetchElevator() {
      try {
        const ref = await firebase.elevator(params.elevatorId).get();
        if (ref.exists) {
          setElevator(ref.data());
        } else {
          toast.error("Wrong elevator ID!");
          history.push("/elevators");
        }
      } catch (err) {
        console.log(err);
        toast.error("Something went wrong:(");
      }
    }
    fetchElevator();
  }, []);

<<<<<<< HEAD
  const deleteFlight = async () => {
=======
  const deleteElevator = async () => {
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d
    try {
      await firebase.elevator(params.elevatorId).delete();
      toast.success("Deleted");
      history.push("/elevators");
    } catch (err) {
      console.log(err);
      toast.error("Something went wrong:(");
    }
  };

  return !elevator ? (
    <Row className="d-flex justify-content-center">
      <Spinner />
    </Row>
  ) : (
    <>
      <Row>
        <h4 className="col-md-12 mt-5 display-4">{elevator.name}</h4>
        <Col xs={12} className="my-2">
          <BoldSpan>Address: </BoldSpan>
          {elevator.address}
        </Col>
        <Col xs={12} className="my-2">
          <BoldSpan>Phone: </BoldSpan>
          {elevator.phone}
        </Col>
        <Col xs={12} className="my-2">
          <BoldSpan>Total Bushel Capacity: </BoldSpan>
          {elevator.capacity}
        </Col>
      </Row>
<<<<<<< HEAD
      <Row>
        <Col className="pr-2" xs={12} md={{ size: 3, offset: 3 }}>
          <Link to={`/elevators/${params.elevatorId}/edit`}>
            <Button block color="primary">
              Edit
            </Button>
          </Link>
        </Col>
        <Col className="pl-2" xs={12} md={{ size: 3 }}>
          <Button onClick={deleteFlight} block color="danger">
            Delete
          </Button>
=======
      {photos && (
        <Row className="my-2">
          {photos.map((photo, i) => (
            <Col key={i} xs={6} md={4} lg={3}>
              <div style={{ position: "relative" }}>
                <img style={{ maxWidth: "100%" }} src={photo.url} />
                <Can I="create" a="Elevators">
                  <PhotoHover>
                    <Button
                      onClick={async () => {
                        await firebase.storage.child(photo.fullPath).delete();
                        setPhotos(
                          photos.filter(
                            filterPhoto =>
                              filterPhoto.fullPath !== photo.fullPath
                          )
                        );
                      }}
                      style={{ position: "absolute", top: "0", right: "10px" }}
                      close
                    />
                  </PhotoHover>
                </Can>
              </div>
            </Col>
          ))}
        </Row>
      )}
      <Can I="create" a="Elevators">
        <Row className="my-2">
          {uploadedPhotos.length > 0 ? (
            <Col xs={6}>
              <Button color="primary" onClick={uploadFiles}>
                Upload photos
              </Button>
            </Col>
          ) : (
            <Col xs={6}>
              <span
                className="btn btn-primary"
                style={{ position: "relative", cursor: "pointer" }}
              >
                Add photos
                <input
                  style={{
                    opacity: 0,
                    position: "absolute",
                    top: 0,
                    left: 0,
                    display: "block",
                    height: "100%",
                    width: "100%",
                    cursor: "pointer"
                  }}
                  multiple
                  accept="image/*"
                  type="file"
                  onChange={handlePhotoInput}
                />
              </span>
            </Col>
          )}
        </Row>
        {uploadedPhotos && (
          <Row>
            {uploadedPhotos.map((photo, i) => (
              <Col key={i} xs={6} md={4} lg={3}>
                <div style={{ position: "relative" }}>
                  <img
                    style={{ maxWidth: "100%" }}
                    src={URL.createObjectURL(photo)}
                  />
                  <PhotoHover>
                    <Button
                      onClick={() => {
                        setuploadedPhotos(
                          uploadedPhotos.filter((_, index) => index !== i)
                        );
                      }}
                      style={{ position: "absolute", top: "0", right: "10px" }}
                      close
                    />
                  </PhotoHover>
                </div>
              </Col>
            ))}
          </Row>
        )}
      </Can>
      <Row>
        <Col className="pr-2" xs={12} md={{ size: 3, offset: 3 }}>
          <Can I="create" a="Elevators">
            {() => (
              <Link to={`/elevators/${params.elevatorId}/edit`}>
                <Button block color="primary">
                  Edit
                </Button>
              </Link>
            )}
          </Can>
        </Col>
        <Col className="pl-2" xs={12} md={{ size: 3 }}>
          <Can I="create" a="Elevators">
            {() => (
              <Button onClick={deleteElevator} block color="danger">
                Delete
              </Button>
            )}
          </Can>
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d
        </Col>
      </Row>
    </>
  );
};

<<<<<<< HEAD
=======
const PhotoHover = styled.div`
  opacity: 0;
  top: 0;
  position: absolute;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.3);
  height: 100%;
  transition: opacity 0.3s linear;
  &:hover {
    opacity: 1;
  }
`;

>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d
export default withFirebase(Elevator);
