import React, { useEffect, useState } from "react";
import * as yup from "yup";
import { Formik } from "formik";
import { StyledForm } from "../../utils/styled";
import { LabelInput } from "../../components/Input";
import { withFirebase } from "../Firebase";
import { toast } from "react-toastify";
<<<<<<< HEAD
import Spinner from "reactstrap/es/Spinner";
import {
  basicRequiredStringValidationSchema,
  dateFutureValidationSchema,
  numberValidationSchema,
=======
import Spinner from "../Spinner";
import {
  basicRequiredStringValidationSchema,
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d
  positiveNumberValidationSchema
} from "../../utils/validations";

const FormValuesValidationSchema = yup.object().shape({
  name: basicRequiredStringValidationSchema,
  address: basicRequiredStringValidationSchema,
  phone: basicRequiredStringValidationSchema,
  capacity: positiveNumberValidationSchema
});

const EditElevator = ({ match: { params }, firebase, history }) => {
  const [values, setValues] = useState(null);

  useEffect(() => {
    async function fetchElevator() {
      try {
        const ref = await firebase.elevator(params.elevatorId).get();
        if (ref.exists) {
          setValues(ref.data());
        } else {
          toast.error("Wrong elevator ID!");
          history.push("/");
        }
      } catch (err) {
        console.log(err);
        toast.error("Something went wrong:(");
      }
    }
    fetchElevator();
  }, []);

  return values ? (
    <div className="flex-grow-1 d-flex flex-row justify-content-center align-items-center">
      <Formik
        enableReinitialize
        initialValues={{
          name: "",
          address: "",
          phone: "",
          capacity: "",
          ...values
        }}
        validationSchema={FormValuesValidationSchema}
        onSubmit={async (
          formValues,
          { setSubmitting, setStatus, setErrors }
        ) => {
          setSubmitting(true);
          try {
            await firebase.elevator(params.elevatorId).set({
              ...formValues
            });
            setSubmitting(false);
            toast.success("Success");
            history.push(`/elevators/${params.elevatorId}`);
          } catch (err) {
            console.log(err);
          }
        }}
      >
        {({ errors, touched, isSubmitting, submitForm }) => (
          <StyledForm style={{ width: "100%" }}>
            <div className="row">
              <LabelInput
                name="name"
                placeholder="Name"
                type="text"
                size={12}
                error={errors.name}
                touched={touched.name}
                label="Name"
              />
              <LabelInput
                name="address"
                className="form-control"
                placeholder="Address"
                type="text"
                size={12}
                error={errors.address}
                touched={touched.address}
                label="Address"
              />
              <LabelInput
                name="phone"
                className="form-control"
                placeholder="phone number"
                type="tel"
                size={12}
                error={errors.phone}
                touched={touched.phone}
                label="Phone "
              />
              <LabelInput
                name="capacity"
                className="form-control"
                placeholder="capacity"
                type="tel"
                size={12}
                error={errors.capacity}
                touched={touched.capacity}
                label="Total Bushel Capacity"
              />
              <div className="col-sm-12 col-md-6 offset-md-3 my-4">
                <button
                  type="submit"
                  className="btn btn-primary btn-block button-text brown-background"
                  disabled={isSubmitting}
                  onClick={submitForm}
                >
                  Update
                </button>
              </div>
            </div>
          </StyledForm>
        )}
      </Formik>
    </div>
  ) : (
    <Spinner />
  );
};

export default withFirebase(EditElevator);
