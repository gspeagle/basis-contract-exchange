import React from "react";
import { Button } from "reactstrap";
import * as yup from "yup";
import { Formik } from "formik";
import { StyledForm } from "../../utils/styled";
import { LabelInput } from "../../components/Input";
import { withFirebase } from "../Firebase";
import { toast } from "react-toastify";
import {
  basicRequiredStringValidationSchema,
  positiveNumberValidationSchema
} from "../../utils/validations";

const FormValuesValidationSchema = yup.object().shape({
  name: basicRequiredStringValidationSchema,
  address: basicRequiredStringValidationSchema,
  phone: basicRequiredStringValidationSchema,
  capacity: positiveNumberValidationSchema
});

const NewElevator = ({ firebase, history }) => {
  return (
    <div className="flex-grow-1 d-flex flex-row justify-content-center align-items-center">
      <Formik
        initialValues={{
          name: "",
          address: "",
          phone: "",
          capacity: ""
        }}
        validationSchema={FormValuesValidationSchema}
        onSubmit={async (values, { setSubmitting, setStatus, setErrors }) => {
          setSubmitting(true);
          const { open, address, phone } = values;
          try {
            const ref = await firebase.elevators().add({ ...values });
            toast.success("Added");
            history.push(`/elevators/${ref.id}`);
          } catch (err) {
            console.log(err);
            toast.error("Something went wrong:(");
          }
          setSubmitting(false);
        }}
      >
        {({ errors, touched, isSubmitting, handleSubmit }) => (
          <StyledForm style={{ width: "100%" }} onSubmit={handleSubmit}>
            <div className="row">
              <LabelInput
                name="name"
                placeholder="Name"
                type="text"
                size={12}
                error={errors.name}
                touched={touched.name}
                label="Name"
              />
              <LabelInput
                name="address"
                className="form-control"
                placeholder="Address"
                type="text"
                size={12}
                error={errors.address}
                touched={touched.address}
                label="Address"
              />
              <LabelInput
                name="phone"
                className="form-control"
                placeholder="phone number"
                type="tel"
                size={12}
                error={errors.phone}
                touched={touched.phone}
                label="Phone "
              />
              <LabelInput
                name="capacity"
                className="form-control"
                placeholder="capacity"
                type="tel"
                size={12}
                error={errors.capacity}
                touched={touched.capacity}
                label="Total Bushel Capacity"
              />
              <div className="col-sm-12 col-md-6 offset-md-3 my-4">
                <button
                  type="submit"
                  className="btn btn-primary btn-block button-text brown-background"
                  disabled={isSubmitting}
                >
                  Create
                </button>
              </div>
            </div>
          </StyledForm>
        )}
      </Formik>
    </div>
  );
};

export default withFirebase(NewElevator);
