import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
<<<<<<< HEAD
import { Row, Button } from "reactstrap";
import { withFirebase } from "../Firebase";
=======
import { Button, Row } from "reactstrap";
import { withFirebase } from "../Firebase";
import { Can } from "../ability";
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d

const ElevatorsList = ({ firebase }) => {
  const [elevators, setElevators] = useState([]);
  useEffect(
    () =>
      firebase.elevators().onSnapshot(querySnapshot => {
        const ref = querySnapshot.docs.map(doc => ({
          id: doc.id,
          ...doc.data()
        }));
        setElevators(ref);
      }),
    []
  );
  return (
    <>
      <Row className="d-flex justify-content-end mt-3 mb-3">
<<<<<<< HEAD
        <Link to="/elevators/new">
          <Button outline>Add Elevator</Button>
        </Link>
=======
        <Can I="create" a="Elevators">
          {() => (
            <Link to="/elevators/new">
              <Button outline>Add Elevator</Button>
            </Link>
          )}
        </Can>
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d
      </Row>
      <Row>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Address</th>
              <th scope="col">Phone</th>
              <th scope="col">Total Bushel Capacity</th>
              <th scope="col" />
            </tr>
          </thead>
          <tbody>
            {elevators &&
              elevators.map((elevator, i) => (
                <tr key={i}>
                  <th scope="row">{i + 1}</th>
                  <td>{elevator.name}</td>
                  <td>{elevator.address}</td>
                  <td>{elevator.phone}</td>
                  <td>{elevator.capacity}</td>
                  <td>
                    <Link to={`/elevators/${elevator.id}`}>
                      <Button outline color="primary">
                        More info
                      </Button>
                    </Link>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </Row>
    </>
  );
};

export default withFirebase(ElevatorsList);
