import React from "react";
import * as yup from "yup";
import {
  emailValidationSchema,
  firstNameValidationSchema,
  lastNameValidationSchema,
  passwordConfirmValidationSchema,
  passwordValidationSchema,
  phoneValidationSchema,
  zipValidationSchema
} from "../utils/validations";
import { Formik } from "formik";
import { StyledForm } from "../utils/styled";
import { Link, withRouter } from "react-router-dom";
import { LabelInput } from "../components/Input";
import Checkbox from "../components/Checkbox";
import { withFirebase } from "../components/Firebase";
import { compose } from "recompose";
import { toast } from "react-toastify";
import { UserRole } from "../stores/UserEnums";

const loginFormValuesValidationSchema = yup.object().shape({
  firstName: firstNameValidationSchema,
  lastName: lastNameValidationSchema,
  email: emailValidationSchema,
  password: passwordValidationSchema,
  passwordConfirm: passwordConfirmValidationSchema,
  zip: zipValidationSchema,
  homePhone: phoneValidationSchema,
  workPhone: phoneValidationSchema,
  cellPhone: phoneValidationSchema
});

const USED_EMAIL = "auth/email-already-in-use";
const roles = [UserRole.ADMIN, UserRole.CLIENT, UserRole.EMPLOYEE];
const signUp = props => (
  <div className="flex-grow-1 d-flex flex-row justify-content-center align-items-center">
    <Formik
      initialValues={{
        email: "",
        password: "",
        passwordConfirm: "",
        subscriptions: ["bulletin", "promotion", "eNews"],
        firstName: "",
        lastName: "",
        company: "",
        role: UserRole.CLIENT,
        address: "",
        city: "",
        state: "",
        zip: "",
        country: "",
        homePhone: "",
        workPhone: "",
        cellPhone: ""
      }}
      validationSchema={loginFormValuesValidationSchema}
      onSubmit={async (values, { setSubmitting, setStatus, setErrors }) => {
        const { email, password, passwordConfirm, ...rest } = values;
        setSubmitting(true);
        const authUser = await props.firebase
          .signUpWithEmailAndPassword(email, password)
          .catch(error => {
            if (error.code === USED_EMAIL) {
              setErrors({ email: "Email is already in use." });
            } else {
              console.log(error.code);
              toast.error("Oops something went wrong");
            }
            return false;
          });

        if (!authUser) {
          setSubmitting(false);
          return;
        }
        await props.firebase.user(authUser.user.uid).set({ email, ...rest });
        const { from } = props.location.state || {
          from: { pathname: "/" }
        };
        props.history.push(from);
        setSubmitting(false);
      }}
    >
      {({ errors, touched, isSubmitting, submitForm }) => (
        <StyledForm style={{ width: "100%" }}>
          <div className="card ">
            <article className="card-body row">
              <div className="col-sm-12">
                <Link
                  to="/signIn"
                  className="float-right btn btn-outline-primary"
                >
                  Sign in
                </Link>
                <h4 className="card-title mb-4 mt-1">Sign up</h4>
                <hr />
                <p className="text-success text-center">Create new account</p>
              </div>
              <LabelInput
                name="email"
                id="email-input"
                placeholder="Email"
                className="form-control"
                type="email"
                size={12}
                error={errors.email}
                touched={touched.email}
                icon="fa-envelope"
                noValidate
              />
              <LabelInput
                name="password"
                id="password-input"
                className="form-control"
                placeholder="Create password"
                type="password"
                error={errors.password}
                touched={touched.password}
                icon="fa-lock"
              />
              <LabelInput
                name="passwordConfirm"
                id="passwordConfirm-input"
                className="form-control"
                placeholder="Repeat password"
                type="password"
                error={errors.passwordConfirm}
                touched={touched.passwordConfirm}
                icon="fa-lock"
              />
              <Checkbox
                name="subscriptions"
                value="bulletin"
                id="bulletin"
<<<<<<< HEAD
                label="News and Updates About Decet"
=======
                label="Follow Decet's Progress"
>>>>>>> master-holder
              />
              <LabelInput
                name="firstName"
                id="firstName-input"
                placeholder="First name"
                className="form-control"
                type="text"
                error={errors.firstName}
                touched={touched.firstName}
                icon="fa-user"
                noValidate
              />
              <LabelInput
                name="lastName"
                id="lastName-input"
                placeholder="Last name"
                className="form-control"
                type="text"
                error={errors.lastName}
                touched={touched.lastName}
                icon="fa-user"
                noValidate
              />
              <LabelInput
                name="company"
                id="company-input"
                placeholder="Company"
                className="form-control"
                type="text"
                error={errors.company}
                touched={touched.company}
                icon="fa-briefcase"
                noValidate
              />
              <LabelInput
                name="role"
                type="select"
                className="form-control"
                error={errors.role}
                touched={touched.role}
                noValidate
                icon="fa-user"
              >
                {roles.map(role => (
                  <option value={role}>{role}</option>
                ))}
              </LabelInput>
              <LabelInput
                name="address"
                id="address-input"
                placeholder="Address"
                className="form-control"
                type="text"
                error={errors.address}
                touched={touched.address}
                icon="fa-home"
                noValidate
              />
              <LabelInput
                name="city"
                id="city-input"
                placeholder="City"
                className="form-control"
                type="text"
                error={errors.city}
                touched={touched.city}
                icon="fa-city"
                noValidate
              />
              <LabelInput
                name="state"
                id="state-input"
                placeholder="State"
                className="form-control"
                type="select"
                error={errors.state}
                touched={touched.state}
                icon="fa-user"
                noValidate
              >
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DE">Delaware</option>
                <option value="DC">District Of Columbia</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="IA">Iowa</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="ME">Maine</option>
                <option value="MD">Maryland</option>
                <option value="MA">Massachusetts</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MS">Mississippi</option>
                <option value="MO">Missouri</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NV">Nevada</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NY">New York</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VT">Vermont</option>
                <option value="VA">Virginia</option>
                <option value="WA">Washington</option>
                <option value="WV">West Virginia</option>
                <option value="WI">Wisconsin</option>
                <option value="WY">Wyoming</option>
              </LabelInput>
              <LabelInput
                name="zip"
                id="zip-input"
                placeholder="Zip code"
                className="form-control"
                type="text"
                error={errors.zip}
                touched={touched.zip}
                icon="fa-map-marker-alt"
                noValidate
              />
              <LabelInput
                name="country"
                id="country-input"
                placeholder="Country"
                className="form-control"
                type="text"
                error={errors.country}
                touched={touched.country}
                icon="fa-flag"
                noValidate
              />
              <LabelInput
                name="homePhone"
                id="homePhone-input"
                placeholder="Home phone"
                className="form-control"
                type="text"
                error={errors.homePhone}
                touched={touched.homePhone}
                icon="fa-phone"
                noValidate
              />
              <LabelInput
                name="workPhone"
                id="workPhone-input"
                placeholder="Work phone"
                className="form-control"
                type="text"
                error={errors.workPhone}
                touched={touched.workPhone}
                icon="fa-building"
                noValidate
              />
              <LabelInput
                name="cellPhone"
                id="cellPhone-input"
                placeholder="Cell phone"
                className="form-control"
                type="text"
                error={errors.cellPhone}
                touched={touched.cellPhone}
                icon="fa-mobile-alt"
                noValidate
              />
              <Checkbox
                name="subscriptions"
                value="promotion"
                id="promotion"
<<<<<<< HEAD
                label="Thoughts and Tangents from the Team"
=======
                label="Thoughts and Tangents From Deceteced"
>>>>>>> master-holder
              />
              <Checkbox
                name="subscriptions"
                value="eNews"
                id="eNews"
<<<<<<< HEAD
                label="Decentralized Marketplaces And Exchange News"
=======
                label="Decentralized Marketplace and Exchange News"
>>>>>>> master-holder
              />
              <div className="form-group col-sm-12 col-md-6 offset-md-3">
                <button
                  type="submit"
                  className="btn btn-primary btn-block"
                  disabled={isSubmitting}
                  onClick={submitForm}
                >
                  Sign up
                </button>
              </div>
              <p className="text-center col-sm-12 col-md-6 offset-md-3">
                Have an account?
                <Link to={"/signIn"}> Sign in</Link>
              </p>
            </article>
          </div>
        </StyledForm>
      )}
    </Formik>
  </div>
);

export default compose(
  withFirebase,
  withRouter
)(signUp);
