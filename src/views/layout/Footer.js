import React from "react";

const Footer = () => (
  <footer className="text-primary p-3">
    <div className="row">
      <div className="col-12 text-center">&copy; 2019 </div>
    </div>
  </footer>
);

export default Footer;
