import React from "react";
import Header from "./Header";
import Footer from "./Footer";
import styled from "styled-components";
import withAuthentication from "../../components/Session/WithAuthentication";

const FullHeight = styled.div`
  min-height: 100%;
`;

const Container = styled.div`
  min-height: calc(100vh - 76px - 56px);
`;

const layout = ({ children }) => (
  <FullHeight>
    <Header />
    <Container className="container">{children} </Container>
    <Footer />
  </FullHeight>
);

export default withAuthentication(layout);
