import {
  Collapse,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink
} from "reactstrap";

import React, { useState } from "react";
import { Link } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { withFirebase } from "../../components/Firebase";
import { compose } from "recompose";
import logo from "../../assets/images/logo.png";
import styled from "styled-components";
import { Can } from "../../components/ability";

const Logo = styled(NavbarBrand)`
  background-image: url(${logo});
  background-size: 60px 60px;
  width: 60px;
  height: 60px;
`;

const header = ({ UserStore, firebase }) => {
  const [isOpen, toggle] = useState(false);
  const { user, isLoggedIn } = UserStore;
  return (
    <Navbar color="light" light expand="md" className="px-lg-5">
      <Logo href="//decet.io" target="_blank" />
      <NavbarBrand tag={Link} to="/">
        Home
      </NavbarBrand>
      <NavbarToggler onClick={() => toggle(!isOpen)} />
      <Collapse isOpen={isOpen} navbar>
        <Nav className="ml-auto" navbar>
          {isLoggedIn ? (
            <>
              <NavItem>
                <NavLink tag={Link} to="/heatmap">
                  Heatmap
                </NavLink>
              </NavItem>
              <Can I="manage" a="Contracts">
                {() => (
                  <>
                    <NavItem>
                      <NavLink tag={Link} to="/contracts/accepted">
                        Accepted contracts
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink tag={Link} to="/contracts/manage">
                        Manage contracts
                      </NavLink>
                    </NavItem>
                  </>
                )}
              </Can>
              <Can not I="manage" a="AllContracts">
                {() => (
                  <NavItem>
                    <NavLink tag={Link} to="/contracts">
                      Contracts
                    </NavLink>
                  </NavItem>
                )}
              </Can>
              <Can I="manage" a="AllContracts">
                {() => (
                  <NavItem>
                    <NavLink tag={Link} to="/allContracts">
                      All contracts
                    </NavLink>
                  </NavItem>
                )}
              </Can>
              <Can I="have" an="Order">
                <NavItem>
                  <NavLink tag={Link} to="/orders">
                    My orders
                  </NavLink>
                </NavItem>
              </Can>
              <Can I="manage" a="Users">
                {() => (
                  <NavItem>
                    <NavLink tag={Link} to="/users">
                      Users
                    </NavLink>
                  </NavItem>
                )}
              </Can>
              <Can I="read" a="Elevators">
                {() => (
                  <NavItem>
                    <NavLink tag={Link} to="/elevators">
                      Elevators
                    </NavLink>
                  </NavItem>
                )}
              </Can>
              <NavItem>
                <NavLink tag={Link} to="/elevators">
                  Elevators
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink disabled>
                  Hello, {user.firstName ? user.firstName : user.email}!
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#" onClick={firebase.signOut}>
                  Sign out
                </NavLink>
              </NavItem>
            </>
          ) : (
            <>
              <NavItem>
                <NavLink tag={Link} to="/signIn">
                  Sign in
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/signUp">
                  Sign up
                </NavLink>
              </NavItem>
            </>
          )}
        </Nav>
      </Collapse>
    </Navbar>
  );
};

export default compose(
  inject("UserStore"),
  withFirebase,
  observer
)(header);
