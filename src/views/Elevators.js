import React from "react";
<<<<<<< HEAD
import { Switch, Route, withRouter } from "react-router-dom";
=======
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d
import ElevatorsList from "../components/elevators/ElevatorsList";
import NewElevator from "../components/elevators/NewElevator";
import Elevator from "../components/elevators/Elevator";
import EditElevator from "../components/elevators/EditElevator";
<<<<<<< HEAD

const Elevators = props => {
  return (
    <Switch>
      <Route exact path="/elevators" component={ElevatorsList} />
      <Route exact path="/elevators/new" component={NewElevator} />
      <Route
        exact
        path="/elevators/:elevatorId/edit"
        component={EditElevator}
      />
      <Route exact path="/elevators/:elevatorId" component={Elevator} />
    </Switch>
  );
};
=======
import { ability } from "../components/ability";

const Elevators = props =>
  ability.can("read", "Elevators") ? (
    <Switch>
      <Route exact path="/elevators" component={ElevatorsList} />
      <Route
        exact
        path="/elevators/new"
        render={routeProps =>
          ability.can("create", "Elevators") ? (
            <NewElevator {...routeProps} />
          ) : (
            <Redirect to="/forbidden" />
          )
        }
      />
      <Route
        exact
        path="/elevators/:elevatorId/edit"
        render={routeProps =>
          ability.can("edit", "Elevators") ? (
            <EditElevator {...routeProps} />
          ) : (
            <Redirect to="/forbidden" />
          )
        }
      />
      <Route exact path="/elevators/:elevatorId" component={Elevator} />
    </Switch>
  ) : (
    <Redirect to="/forbidden" />
  );
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d

export default withRouter(Elevators);
