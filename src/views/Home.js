import React from "react";
import { Link } from "react-router-dom";

const home = props => (
  <>
    <p>Home</p>
    <br />
    <Link to={"/contracts"}>Contracts</Link>
  </>
);

export default home;
