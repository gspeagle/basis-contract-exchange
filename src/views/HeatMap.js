import React from "react";
import { Map, TileLayer } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import HeatmapLayer from "react-leaflet-heatmap-layer";

const HeatMap = props => {
  const position = [34.08, -118.31];
  const points = [
    { lat: 34.08, lng: -118.31, count: 1000 },
    { lat: 34.08, lng: -118.3, count: 500 },
    { lat: 34.08, lng: -118.11, count: 600 },
    { lat: 34.08, lng: -118.331, count: 2000 }
  ];
  return (
    <div className="pt-5">
      <Map center={position} zoom={5}>
        <HeatmapLayer
          fitBoundsOnLoad
          fitBoundsOnUpdate
          max={10}
          points={points}
          longitudeExtractor={m => m.lng}
          blur={20}
          latitudeExtractor={m => m.lat}
          intensityExtractor={m => m.count}
        />
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
      </Map>
    </div>
  );
};

export default HeatMap;
