import { Link } from "react-router-dom";
import React from "react";
import styled from "styled-components";
import { Formik } from "formik";
import moment from "moment";
import * as yup from "yup";
import {
  afterDateValidationSchema,
  dateFutureValidationSchema,
  positiveIntegerValidationSchema,
  positiveNumberValidationSchema,
  stringMaxValidationSchema,
  textMaxValidationSchema
} from "../../utils/validations";
import { withFirebase } from "../../components/Firebase";
import { compose } from "recompose";
import { inject } from "mobx-react";
import { toast } from "react-toastify";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";

const offerFormValuesValidationSchema = yup.object().shape({
  quantity: positiveIntegerValidationSchema,
  bushels: positiveNumberValidationSchema,
  deliveryBetweenDate: dateFutureValidationSchema,
  deliveryAndDate: afterDateValidationSchema("deliveryBetweenDate"),
  expirationDate: dateFutureValidationSchema,
  contact1: stringMaxValidationSchema,
  contact2: stringMaxValidationSchema,
  comments: textMaxValidationSchema
});

export const YEAR_FORMAT = "YYYY-MM-DD";
export const TIME_FORMAT = "HH:mm";

const offerForm = ({ contract, firebase, UserStore }) => (
  <Formik
    initialValues={{
      commodityName: contract.commodityName,
      offerType: "sell",
      contractType: "cash",
      quantity: 5000,
      bushels: 3.18,
      deliveryBetweenDate: moment().format(YEAR_FORMAT),
      deliveryBetweenTime: moment().format(TIME_FORMAT),
      deliveryAndDate: moment()
        .add(7, "days")
        .format(YEAR_FORMAT),
      deliveryAndTime: moment()
        .add(1, "hours")
        .format(TIME_FORMAT),
      expirationDate: moment()
        .add(1, "days")
        .format(YEAR_FORMAT),
      expirationTime: moment().format(TIME_FORMAT),
      deliveryType: "delivered",
      contact1: "",
      contact2: "",
      comments: ""
    }}
    validationSchema={offerFormValuesValidationSchema}
    onSubmit={async (values, { setSubmitting, setStatus, setErrors }) => {
      setSubmitting(true);
      const {
        deliveryBetweenDate,
        deliveryBetweenTime,
        deliveryAndDate,
        deliveryAndTime,
        expirationDate,
        expirationTime,
        ...rest
      } = values;
      let deliveryBetween = moment(
        deliveryBetweenDate + deliveryBetweenTime,
        YEAR_FORMAT + TIME_FORMAT
      );
      let deliveryAnd = moment(
        deliveryAndDate + deliveryAndTime,
        YEAR_FORMAT + TIME_FORMAT
      );
      if (deliveryAnd <= deliveryBetween) {
        setErrors({ deliveryAndTime: "Should be after Between" });
        return;
      }
      deliveryBetween = deliveryBetween.toISOString();
      deliveryAnd = deliveryAnd.toISOString();
      const expiration = moment(
        expirationDate + expirationTime,
        YEAR_FORMAT + TIME_FORMAT
      ).toISOString();

      await firebase.db.collection(`offers`).add({
        deliveryBetween,
        deliveryAnd,
        expiration,
        location: contract.location,
        contractId: contract.id,
        uid: UserStore.user.uid,
        ...rest
      });

      toast.success("Offer added!");
      setStatus({ submitted: true });
      setSubmitting(false);
    }}
  >
    {({
      errors,
      touched,
      isSubmitting,
      submitForm,
      handleSubmit,
      values,
      status,
      handleChange,
      handleBlur
    }) => {
      return status && status.submitted ? (
        <p>Thank you for submitting your cash bid offer.</p>
      ) : (
        <Grid item sm={12} md={6}>
          <form onSubmit={handleSubmit}>
            <FormInput>
              <InputGrid>
                <Grid item sm={6}>
                  Commodity:
                </Grid>

                <Grid item sm={6}>
                  {values.commodityName}
                </Grid>
              </InputGrid>
            </FormInput>

            <FormInput>
              <InputGrid>
                <Grid item sm={6}>
                  <InputLabel> Buy or Sell: </InputLabel>
                </Grid>
                <Grid item sm={6}>
                  <Select
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.offerType}
                    error={!!(touched.offerType && errors.offerType)}
                    inputProps={{ name: "offerType" }}
                  >
                    <MenuItem value="buy">Buy</MenuItem>
                    <MenuItem value="sell">Sell</MenuItem>
                  </Select>
                </Grid>
              </InputGrid>
            </FormInput>

            <FormInput>
              <InputGrid>
                <Grid item sm={6}>
                  <InputLabel> Contract Type: </InputLabel>
                </Grid>
                <Grid item sm={6}>
                  <Select
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.contractType}
                    inputProps={{ name: "contractType" }}
                    error={!!(touched.contractType && errors.contractType)}
                  >
                    <MenuItem value="cash">Cash</MenuItem>
                    <MenuItem value="futures">Futures only</MenuItem>
                    <MenuItem value="basis">Basis only</MenuItem>
                  </Select>
                </Grid>
              </InputGrid>
            </FormInput>

            <FormInput>
              <InputGrid>
                <Grid item sm={6}>
                  <InputLabel> Quantity: </InputLabel>
                </Grid>
                <Grid item sm={6}>
                  <NumberInput
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.quantity}
                    inputProps={{ name: "quantity" }}
                    error={!!(touched.quantity && errors.quantity)}
                    helperText={touched.quantity && errors.quantity}
                  />
                </Grid>
              </InputGrid>
            </FormInput>

            <FormInput>
              <InputGrid>
                <Grid item sm={6}>
                  <InputLabel> Price Bushels at: </InputLabel>
                </Grid>
                <Grid item sm={6}>
                  <NumberInput
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.bushels}
                    error={!!(touched.bushels && errors.bushels)}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">$</InputAdornment>
                      ),
                      name: "bushels"
                    }}
                    helperText={touched.bushels && errors.bushels}
                  />
                </Grid>
              </InputGrid>
            </FormInput>

            <FormInput>Delivery:</FormInput>

            <FormInput>
              <IndentedInputGrid>
                <Grid item xs={12} sm={4}>
                  <IndentedInputLabel> Between</IndentedInputLabel>
                </Grid>

                <Grid item xs={12} sm={4}>
                  <TextField
                    type="date"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    helperText={
                      touched.deliveryBetweenDate && errors.deliveryBetweenDate
                    }
                    error={
                      !!(
                        touched.deliveryBetweenDate &&
                        errors.deliveryBetweenDate
                      )
                    }
                    InputProps={{ name: "deliveryBetweenDate" }}
                    value={values.deliveryBetweenDate}
                  />
                </Grid>

                <Grid item xs={12} sm={4}>
                  <TextField
                    type="time"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    style={{ position: "relative", left: "50px" }}
                    InputProps={{ name: "deliveryBetweenTime" }}
                    value={values.deliveryBetweenTime}
                    error={
                      !!(touched.deliveryAndTime && errors.deliveryBetweenTime)
                    }
                    helperText={
                      touched.deliveryAndTime && errors.deliveryBetweenTime
                    }
                  />
                </Grid>
              </IndentedInputGrid>
            </FormInput>

            <FormInput>
              <IndentedInputGrid>
                <Grid item xs={12} sm={4}>
                  <IndentedInputLabel> And</IndentedInputLabel>
                </Grid>

                <Grid item xs={12} sm={4}>
                  <TextField
                    type="date"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    style={{ width: "150px" }}
                    InputProps={{ name: "deliveryAndDate" }}
                    value={values.deliveryAndDate}
                    error={
                      !!(touched.deliveryAndDate && errors.deliveryAndDate)
                    }
                    helperText={
                      touched.deliveryAndDate && errors.deliveryAndDate
                    }
                  />
                </Grid>

                <Grid item xs={12} sm={4}>
                  <TextField
                    type="time"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    style={{ position: "relative", left: "50px" }}
                    InputProps={{ name: "deliveryAndTime" }}
                    value={values.deliveryAndTime}
                    error={
                      !!(touched.deliveryAndTime && errors.deliveryAndTime)
                    }
                    helperText={
                      touched.deliveryAndTime && errors.deliveryAndTime
                    }
                  />
                </Grid>
              </IndentedInputGrid>
            </FormInput>

            <FormInput>
              <IndentedInputGrid>
                <Grid item xs={12} sm={4}>
                  <InputLabel> Offer Expires</InputLabel>
                </Grid>

                <Grid item xs={12} sm={4}>
                  <TextField
                    onChange={handleChange}
                    onBlur={handleBlur}
                    type="date"
                    name="expirationDate"
                    style={{ width: "150px" }}
                    placeholder="between"
                    error={!!(touched.expirationDate && errors.expirationDate)}
                    value={values.expirationDate}
                    helperText={touched.expirationDate && errors.expirationDate}
                  />
                </Grid>

                <Grid item xs={12} sm={4}>
                  <TextField
                    onChange={handleChange}
                    onBlur={handleBlur}
                    type="time"
                    name="expirationTime"
                    style={{ position: "relative", left: "50px" }}
                    error={!!(touched.expirationTime && errors.expirationTime)}
                    value={values.expirationTime}
                    helperText={touched.expirationTime && errors.expirationTime}
                  />
                </Grid>
              </IndentedInputGrid>
            </FormInput>

            <FormInput>
              <InputGrid>
                <Grid item sm={6}>
                  <InputLabel> Delivery method: </InputLabel>
                </Grid>
                <Grid item sm={6}>
                  <Select
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.deliveryType}
                    inputProps={{ name: "deliveryType" }}
                    error={!!(touched.deliveryType && errors.deliveryType)}
                  >
                    <MenuItem value="delivered">Delivered</MenuItem>
                    <MenuItem value="fob">On farm pickup(FOB)</MenuItem>
                  </Select>
                </Grid>
              </InputGrid>
            </FormInput>

            <FormInput>
              <TextInputGrid>
                <Grid item sm={6}>
                  <InputLabel> Contact 1: </InputLabel>
                </Grid>
                <Grid item sm={6}>
                  <TextField
                    type="text"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    inputProps={{ name: "contact1" }}
                    style={{
                      width: "100%",
                      position: "relative",
                      right: "100px"
                    }}
                    error={!!(touched.contact1 && errors.contact1)}
                    value={values.contact1}
                    helperText={touched.contact1 && errors.contact1}
                  />
                </Grid>
              </TextInputGrid>
            </FormInput>

            <FormInput>
              <TextInputGrid>
                <Grid item sm={6}>
                  <InputLabel> Contact 2: </InputLabel>
                </Grid>
                <Grid item sm={6}>
                  <TextField
                    type="text"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    inputProps={{ name: "contact2" }}
                    style={{
                      width: "100%",
                      position: "relative",
                      right: "100px"
                    }}
                    error={!!(touched.contact2 && errors.contact2)}
                    value={values.contact2}
                    helperText={touched.contact2 && errors.contact2}
                  />
                </Grid>
              </TextInputGrid>
            </FormInput>

            <FormInput>
              <TextInputGrid>
                <Grid item sm={6}>
                  <InputLabel> Comments: </InputLabel>
                </Grid>
                <Grid item sm={6}>
                  <TextField
                    type="text"
                    multiline={true}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    inputProps={{ name: "comments" }}
                    style={{
                      width: "100%",
                      position: "relative",
                      right: "100px"
                    }}
                    error={!!(touched.comments && errors.comments)}
                    value={values.comments}
                  />
                </Grid>
              </TextInputGrid>
            </FormInput>

            <InputGrid>
              <Grid item sm={6}>
                <Button
                  variant="outlined"
                  color="primary"
                  type="submit"
                  disabled={isSubmitting}
                >
                  Submit
                </Button>
              </Grid>
              <Grid item sm={6}>
                <Link to="/contracts">
                  <Button variant="outlined" color="secondary">
                    Cancel
                  </Button>
                </Link>
              </Grid>
            </InputGrid>
          </form>
        </Grid>
      );
    }}
  </Formik>
);

const NumberInput = styled(TextField).attrs(props => ({
  type: "number"
}))`
  width: 40%;
`;
const Header = styled(Typography).attrs(() => ({
  variant: "h3"
}))`
  margin-bottom: 25px !important;
`;

const InputGrid = styled(Grid).attrs(props => ({
  container: true,
  spacing: 1
}))`
  width: 50% !important;
`;

const Grid16pxMargin = styled(Grid).attrs(props => ({
  item: true,
  md: 3,
  lg: 3
}))`
  margin-bottom: 16px !important;
`;

const TextInputGrid = styled(InputGrid)`
  width: 80% !important;
`;

const IndentedInputGrid = styled(Grid).attrs(props => ({
  container: true,
  spacing: 1
}))`
  width: 75% !important;
`;

const DataContainer = styled.div``;
const BoldSpan = styled.span`
  font-weight: 700;
`;

const BoldSpanWithMargin = styled(BoldSpan)`
  margin-bottom: 24px;
`;
const InputLabel = styled.span`
  position: relative;
  top: 8px;
  text-align: left;
`;
const DataDiv = styled.div`
  text-align: left;
  margin-left: 24px;
  margin-bottom: 16px;
  font-size: 125%;
`;

const FormInput = styled.div`
  margin-bottom: 16px;
  text-align: left;
`;

const IndentedInputLabel = styled.span`
  margin-right: 4em;
  position: relative;
  top: 3px;
  left: 24px;
`;

export default compose(
  withFirebase,
  inject("UserStore")
)(offerForm);
