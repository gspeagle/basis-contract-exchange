import React, { useEffect, useState } from "react";
import Grid from "@material-ui/core/Grid";
import { Button, Col, Row } from "reactstrap";
import withAuthorization from "../../components/Session/WithAuthorization";
import { inject, observer } from "mobx-react";
import { compose } from "recompose";
import { Link, withRouter } from "react-router-dom";
import OfferForm from "./OfferForm";
import ContractInfo from "../../components/Contracts/ContractInfo";
import { toast } from "react-toastify";
import { withFirebase } from "../../components/Firebase";
import Spinner from "../../components/Spinner";

const offer = ({ match: { params }, firebase, history }) => {
  const [contract, setContract] = useState(null);
  useEffect(
    () =>
      firebase.contract(params.contractId).onSnapshot(doc => {
        if (doc.exists) {
          setContract({ ...doc.data(), id: doc.id });
        } else {
          toast.error("Wrong contract ID!");
          history.push("/");
        }
      }),
    []
  );
  return contract ? (
    <>
      <Button
        className="mt-2"
        outline
        onClick={history.goBack}
        color="primary"
        size="sm"
      >
        Back
      </Button>

      <Grid container spacing={2}>
        <ContractInfo contract={contract} />
        <OfferForm contract={contract} />
      </Grid>
    </>
  ) : (
    <Spinner />
  );
};

export default compose(
  withAuthorization,
  withRouter,
  withFirebase
)(offer);
