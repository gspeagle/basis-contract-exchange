import React, { useEffect } from "react";

const Contract = props => {
  return (
    <div>
      Contract
      <button>Deploy</button>
      <button onClick={invokeContract}>Invoke</button>
    </div>
  );
};

const account = {
  mnemonic: [
    "drop",
    "crumble",
    "memory",
    "during",
    "coyote",
    "wrist",
    "dynamic",
    "donkey",
    "peanut",
    "palace",
    "buffalo",
    "mammal",
    "super",
    "clog",
    "fish"
  ],
  pkh: "tz1TYhmuZw5gEbhMikiM8eMCK9UVU1GRPS3Q",
  password: "smboLEm1b8",
  email: "mfrqmyoe.duzmhuuf@tezos.example.org"
};

const getKeystore = async account =>
  await conseiljs.TezosWalletUtil.unlockFundraiserIdentity(
    account.mnemonic.join(" "),
    account.email,
    account.password,
    account.pkh
  );

const tezosNode = "http://alphanet-node.tzscan.io:80";

async function invokeContract() {
  const keystore = await getKeystore(account);
  const contractAddress = "KT1WAjcKUV3igeVhuLsBpZYRgwRSvoeQ82Ki";
  const contractParameters = `parameter (or (or (or (unit %confirm_delivery) (unit %deposit)) (unit %send_money_to_elevator)) (bool %test));`;
  const entryPoints = await conseiljs.TezosContractIntrospector.generateEntryPointsFromParams(
    contractParameters
  );
  const result = await conseiljs.TezosNodeWriter.sendContractInvocationOperation(
    tezosNode,
    keystore,
    contractAddress,
    10000,
    100000,
    "",
    1000,
    100000,
    entryPoints[3].generateParameter("True"),
    conseiljs.TezosParameterFormat.Michelson
  );

  console.log(result.results.contents[0].metadata);
}

export default Contract;
