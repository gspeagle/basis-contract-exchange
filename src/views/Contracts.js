import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import withAuthorization from "../components/Session/WithAuthorization";
import { compose } from "recompose";
import {
  Contract,
  ContractsContainer,
  EditContract,
  NewContract
} from "../components/Contracts";
import { ability } from "../components/ability";
import AcceptedContractsContainer from "../components/Contracts/accepted/AcceptedContractsContainer";
import AcceptedContract from "../components/Contracts/accepted/AcceptedContract";
import AcceptedOffer from "../components/Contracts/accepted/AcceptedOffer";

const ContractRoutes = () => (
  <Switch>
    <Route exact path="/contracts" component={ContractsContainer} />
    <Route
      exact
      path="/contracts/manage"
      render={routeProps =>
        ability.can("manage", "Contracts") ? (
          <ContractsContainer {...routeProps} isManagement={true} />
        ) : (
          <Redirect to="/forbidden" />
        )
      }
    />
    <Route exact path="/contracts/new" component={NewContract} />
    <Route
      exact
      path="/contracts/accepted"
      component={AcceptedContractsContainer}
    />
    <Route
      exact
      path="/contracts/accepted/:contractId"
      component={AcceptedContract}
    />
    <Route
      exact
      path="/contracts/acceptedOffer/:offerId"
      component={AcceptedOffer}
    />
    <Route exact path="/contracts/:contractId" component={Contract} />
    <Route exact path="/contracts/:contractId/edit" component={EditContract} />
  </Switch>
);

export default compose(withAuthorization)(ContractRoutes);
