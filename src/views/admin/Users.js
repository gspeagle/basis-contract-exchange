import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import withAuthorization from "../../components/Session/WithAuthorization";
import { compose } from "recompose";
import { User, UsersContainer, EditUser } from "../../components/admin/Users";
import { ability } from "../../components/ability";

const UserRoutes = () =>
  ability.can("manage", "Users") ? (
    <Switch>
      <Route exact path="/users" component={UsersContainer} />
      <Route exact path="/users/:userId" component={User} />
      <Route exact path="/users/:userId/edit" component={EditUser} />
    </Switch>
  ) : (
    <Redirect to="/forbidden" />
  );

export default compose(withAuthorization)(UserRoutes);
