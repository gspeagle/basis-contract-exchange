import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import withAuthorization from "../../components/Session/WithAuthorization";
import { compose } from "recompose";
import {
  Contract,
  ContractsContainer,
  EditContract,
  NewContract
} from "../../components/admin/Contracts";
import { ability } from "../../components/ability";

const ContractRoutes = () =>
  ability.can("manage", "AllContracts") ? (
    <Switch>
      <Route exact path="/allContracts" component={ContractsContainer} />
      <Route exact path="/allContracts/new" component={NewContract} />
      <Route exact path="/allContracts/:contractId" component={Contract} />
      <Route
        exact
        path="/allContracts/:contractId/edit"
        component={EditContract}
      />
    </Switch>
  ) : (
    <Redirect to="/forbidden" />
  );

export default compose(withAuthorization)(ContractRoutes);
