import React, { useState } from "react";
import { Button, Table } from "reactstrap";
import moment from "moment";
import Modal from "../../components/Modal";
import Spinner from "../../components/Spinner";
import { OrderTypes } from "../../constants/OrdersTypeEnum";
import { Link } from "react-router-dom";

const Orders = ({ loading, orders, type, action, onAccept }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [toDeleteOffer, setToDeleteOffer] = useState(null);
  const [toAcceptOffer, setToAcceptOffer] = useState();
  const [isAcceptModalOpen, setAcceptModalOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const showModal = offer => {
    setToDeleteOffer(offer);
    toggle();
  };

  const toggleAcceptModal = () => {
    setAcceptModalOpen(!isAcceptModalOpen);
  };

  const showAcceptModal = offer => {
    setToAcceptOffer(offer);
    toggleAcceptModal();
  };

  return (
    <>
      <h4 className="text-center pt-5">{type}</h4>
      {loading ? (
        <Spinner />
      ) : (
        <Table hover responsive size="sm">
          <thead>
            <tr>
              <th>Id</th>
              <th>Commodity</th>
              <th>Location</th>
              {type !== OrderTypes.PRIVATE && <th>Type</th>}
              <th>Delivery by</th>
              <th>Price</th>
              <th>Units</th>
              {type !== OrderTypes.PRIVATE && <th>Status</th>}
              <th>Expires</th>
              {type !== OrderTypes.CLOSED && <th>Action</th>}
            </tr>
          </thead>
          <tbody>
            {orders.map(data => (
              <tr key={data.id}>
                <th scope="row">{data.id}</th>
                <td>{data.commodityName}</td>
                <td>{data.location}</td>
                {type !== OrderTypes.PRIVATE && <td>{data.offerType}</td>}
                <td>{moment(data.deliveryAnd).format("MMM DD h:mm A")}</td>
                <td>{`$${data.bushels}`}</td>
                <td>{data.quantity}</td>
                {type !== OrderTypes.PRIVATE && <td>{data.status}</td>}
                <td>{moment(data.expiration).format("MMM DD h:mm A")}</td>
                {type === OrderTypes.OPEN && (
                  <td>
                    <Button
                      outline
                      color="danger"
                      size="sm"
                      onClick={() => showModal(data)}
                    >
                      Cancel
                    </Button>
                    {/* <Button
                      outline
                      color="primary"
                      size="sm"
                      onClick={() => showAcceptModal(data)}
                    >
                      Accept
                    </Button> */}
                  </td>
                )}
                {type === OrderTypes.PRIVATE && (
                  <td>
                    <Button
                      tag={Link}
                      to={`/contracts/acceptedOffer/${data.id}`}
                      outline
                      color="primary"
                      size="sm"
                    >
                      More info
                    </Button>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </Table>
      )}
      {type === OrderTypes.OPEN && (
        <>
          <Modal
            okAction={() => {
              onAccept(toAcceptOffer.id);
              toggleAcceptModal();
            }}
            isOpen={isAcceptModalOpen}
            toggle={toggleAcceptModal}
            body={"Do you want accept this offer"}
            titile={`Offer ${toAcceptOffer && toAcceptOffer.id}`}
          />
          <Modal
            okAction={() => {
              action(toDeleteOffer);
              toggle();
            }}
            isOpen={isOpen}
            toggle={toggle}
            body={"Do you want cancel this offer"}
            titile={`Offer ${toDeleteOffer && toDeleteOffer.id}`}
          />
        </>
      )}
    </>
  );
};

export default Orders;
