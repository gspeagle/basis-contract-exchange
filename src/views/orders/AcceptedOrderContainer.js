import React, { useEffect, useState } from "react";
import { compose } from "recompose";
import withAuthorization from "../../components/Session/WithAuthorization";
import Orders from "./Orders";
import { OrderTypes } from "../../constants/OrdersTypeEnum";

const AcceptedOrdersContainer = ({ firebase, UserStore }) => {
  const [closedOrders, setClosedOrders] = useState(undefined);
  useEffect(
    () =>
      firebase.closedOrders(UserStore.user.uid).onSnapshot(querySnapshot => {
        const orders = querySnapshot.docs.map(doc => {
          return { id: doc.id, ...doc.data() };
        });
        console.log(orders);
        setClosedOrders(orders);
      }),
    []
  );
  console.log(!closedOrders, closedOrders);
  return (
    <Orders
      loading={!closedOrders}
      type={OrderTypes.ACCEPTED}
      openOrders={closedOrders}
    />
  );
};

export default compose(withAuthorization)(AcceptedOrdersContainer);
