import React, { useEffect, useState } from "react";
import { compose } from "recompose";
import withAuthorization from "../../components/Session/WithAuthorization";
import Orders from "./Orders";
import { OrderTypes } from "../../constants/OrdersTypeEnum";

const container = ({ firebase, UserStore }) => {
  const [closedOrders, setClosedOrders] = useState(undefined);
  useEffect(
    () =>
      firebase.closedOrders(UserStore.user.uid).onSnapshot(querySnapshot => {
        const openOrders = querySnapshot.docs.map(doc => {
          return { id: doc.id, ...doc.data() };
        });
        setClosedOrders(openOrders);
      }),
    []
  );
  return (
    <Orders
      loading={!closedOrders}
      type={OrderTypes.CLOSED}
      orders={closedOrders}
    />
  );
};

export default compose(withAuthorization)(container);
