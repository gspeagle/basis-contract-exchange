import React, { useEffect, useState } from "react";
import { compose } from "recompose";
import withAuthorization from "../../components/Session/WithAuthorization";
import Orders from "./Orders";
import { OfferStatus } from "../../constants/OfferStatusEnum";
import { OrderTypes } from "../../constants/OrdersTypeEnum";

const container = ({ firebase, UserStore }) => {
  const [openOrders, setOpenOrders] = useState(undefined);

  const action = async offer => {
    await firebase.offer(offer.id).update({ status: OfferStatus.CANCELED });
  };

  useEffect(
    () =>
      firebase.openOrders(UserStore.user.uid).onSnapshot(querySnapshot => {
        const openOrders = querySnapshot.docs.map(doc => {
          return { id: doc.id, ...doc.data() };
        });
        setOpenOrders(openOrders);
      }),
    []
  );

  return (
    <Orders
      action={action}
      loading={!openOrders}
      type={OrderTypes.OPEN}
      orders={openOrders}
    />
  );
};

export default compose(withAuthorization)(container);
