import React, { useEffect, useState } from "react";
import { compose } from "recompose";
import withAuthorization from "../../components/Session/WithAuthorization";
import Orders from "./Orders";
import { OrderTypes } from "../../constants/OrdersTypeEnum";

const container = ({ firebase, UserStore }) => {
  const [privateOrders, setPrivateOrders] = useState(undefined);
  useEffect(
    () =>
      firebase.privateOrders(UserStore.user.uid).onSnapshot(querySnapshot => {
        const openOrders = querySnapshot.docs.map(doc => {
          return { id: doc.id, ...doc.data() };
        });
        console.log(openOrders);
        setPrivateOrders(openOrders);
      }),
    []
  );
  return (
    <Orders
      loading={!privateOrders}
      type={OrderTypes.PRIVATE}
      orders={privateOrders}
    />
  );
};

export default compose(withAuthorization)(container);
