import React from "react";
import PrivateOrderContainer from "./PrivateOrderContainer";
import ClosedOrderContainer from "./ClosedOrderContainer";
import OpenOrderContainer from "./OpenOrderContainer";

const orders = props => (
  <>
    <PrivateOrderContainer className="mt-5" />
    <OpenOrderContainer />
    <ClosedOrderContainer />
  </>
);

export default orders;
