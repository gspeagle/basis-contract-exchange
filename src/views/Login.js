import React from "react";
import * as yup from "yup";
import {
  basicRequiredStringValidationSchema,
  emailValidationSchema
} from "../utils/validations";
import { Formik } from "formik";
import { StyledForm } from "../utils/styled";
import { Link, withRouter } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { LabelInput } from "../components/Input";
import { withFirebase } from "../components/Firebase";
import { compose } from "recompose";
import { toast } from "react-toastify";

const loginFormValuesValidationSchema = yup.object().shape({
  email: emailValidationSchema,
  password: basicRequiredStringValidationSchema
});
const USER_NOT_FOUND = "auth/user-not-found";
const WRONG_PASSWORD = "auth/wrong-password";
const ACCOUNT_DISABLED = "auth/user-disabled";

const login = props => (
  <div className="flex-grow-1 d-flex flex-row justify-content-center align-items-center">
    <Formik
      initialValues={{ email: "", password: "" }}
      validationSchema={loginFormValuesValidationSchema}
      onSubmit={async (values, { setSubmitting, setStatus, setErrors }) => {
        setSubmitting(true);
        const { email, password } = values;

        const auth = await props.firebase
          .signInWithEmailAndPassword(email, password)
          .catch(error => {
            if (error.code === USER_NOT_FOUND) {
              setErrors({ email: "User not found" });
            } else if (error.code === WRONG_PASSWORD) {
              setErrors({ password: "Wrong password" });
            } else if (error.code === ACCOUNT_DISABLED) {
              setErrors({ password: "Your account was disabled" });
            } else {
              console.log(error);
              toast.error("Oops something went wrong");
            }
            return false;
          });
        setSubmitting(false);
        if (auth) {
          const { from } = props.location.state || {
            from: { pathname: "/" }
          };
          props.history.push(from);
        }
      }}
    >
      {({ errors, touched, isSubmitting, submitForm }) => (
        <StyledForm>
          <div className="card">
            <article className="card-body">
              <Link
                to="/signUp"
                className="float-right btn btn-outline-primary"
              >
                Sign up
              </Link>
              <h4 className="card-title mb-4 mt-1">Sign in</h4>
              <hr />
              <p className="text-success text-center">
                Enter email and password
              </p>
              <LabelInput
                name="email"
                id="email-input"
                placeholder="Email"
                className="form-control"
                type="email"
                error={errors.email}
                touched={touched.email}
                label="Your email"
                icon="fa-user"
                size={12}
                noValidate
              />
              <LabelInput
                name="password"
                id="password-input"
                className="form-control"
                placeholder="******"
                type="password"
                error={errors.password}
                touched={touched.password}
                label="Your password"
                icon="fa-lock"
                size={12}
              />
              <div className="form-group">
                <button
                  type="submit"
                  className="btn btn-primary btn-block"
                  disabled={isSubmitting}
                  onClick={submitForm}
                >
                  Sign in
                </button>
              </div>
              <p className="text-center">
                <Link to={"/forgot-password"}>Forgot password?</Link>
              </p>
            </article>
          </div>
        </StyledForm>
      )}
    </Formik>
  </div>
);

export default compose(
  withRouter,
  withFirebase,
  inject("UserStore"),
  observer
)(login);
