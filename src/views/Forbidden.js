import React from "react";

const forbidden = props => {
  return "Forbidden. Access denied.";
};

export default forbidden;
