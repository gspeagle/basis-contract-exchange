import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Home from "./views/Home";
import Login from "./views/Login";
import NotFound from "./views/NotFound";
import SignUp from "./views/SignUp";
import Offer from "./views/offer";
import Orders from "./views/orders";
import { inject, observer } from "mobx-react";
import { compose } from "recompose";
import Contracts from "./views/Contracts";
import Elevators from "./views/Elevators";
<<<<<<< HEAD
=======
import Forbidden from "./views/Forbidden";
import AllContracts from "./views/admin/AllContracts";
import Users from "./views/admin/Users";
import HeatMap from "./views/HeatMap";
import Contract from "./views/Contract";
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d

const routes = ({ UserStore }) => (
  <Switch>
    <Route exact path="/" component={Home} />} />
    <Route
      path="/signIn"
      render={() => (UserStore.isLoggedIn ? <Redirect to="/" /> : <Login />)}
    />
    <Route
      path="/signUp"
      render={() => (UserStore.isLoggedIn ? <Redirect to="/" /> : <SignUp />)}
    />
    <Route path="/contracts" component={Contracts} />
<<<<<<< HEAD
=======
    <Route path="/allContracts" component={AllContracts} />
    <Route path="/users" component={Users} />
>>>>>>> 7a059c811a6537dbbded0e08e197aaa2bbf69b5d
    <Route path="/elevators" component={Elevators} />
    <Route path="/offer/:contractId" component={Offer} />
    <Route path="/orders" component={Orders} />
    <Route path="/heatmap" component={HeatMap} />
    <Route path="/contract" component={Contract} />
    <Route path="/forbidden" component={Forbidden} />
    <Route component={NotFound} />
  </Switch>
);

export default compose(
  inject("UserStore"),
  observer
)(routes);
