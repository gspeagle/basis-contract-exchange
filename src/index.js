import React from "react";
import ReactDOM from "react-dom";
import Routes from "./Routes";
import { Provider } from "mobx-react";
import { BrowserRouter } from "react-router-dom";
import * as serviceWorker from "./serviceWorker";
import UserStore from "./stores/UserStore";
import Layout from "./views/layout";
import Firebase, { FirebaseContext } from "./components/Firebase";
import ErrorBoundry from "./views/ErrorBoundry";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Styles.css";
import { toast, ToastContainer } from "react-toastify";
import { AbilityContext, ability } from "./components/ability";

const App = () => (
  <ErrorBoundry>
    <BrowserRouter>
      <Provider UserStore={UserStore}>
        <FirebaseContext.Provider value={new Firebase()}>
          <AbilityContext.Provider value={ability}>
            <Layout>
              <Routes />
            </Layout>
          </AbilityContext.Provider>
        </FirebaseContext.Provider>
      </Provider>
    </BrowserRouter>
    <ToastContainer
      autoClose={8000}
      hideProgressBar={false}
      closeOnClick={true}
      pauseOnHover={true}
      draggable={true}
    />
  </ErrorBoundry>
);

ReactDOM.render(<App />, document.getElementById("root"));

const config = {
  onUpdate: registration => {
    toast.success(
      "WOW! New content is available and will be used when all tabs for this page are closed.!",
      {
        position: "top-right",
        autoClose: 10000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      }
    );
  }
};
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register(config);
