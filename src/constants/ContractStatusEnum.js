export const ContractStatus = {
  OPEN: "open",
  ACCEPTED: "ACCEPTED",
  CLOSED: "CLOSED"
};
