import { Form } from "formik";
import styled from "styled-components";
import { Col } from "reactstrap";

export const StyledForm = styled(Form)`
  max-width: 720px;
  width: 400px;
  margin-top: 24px;
`;

export const StyledIconSpan = styled.span`
  display: block !important;
  margin: auto;
  min-width: 48px;
  text-align: center;
`;

export const BoldSpan = styled.span`
  font-weight: bold;
`;

export const StyledCol = styled(Col)`
  margin-top: 16px;
`;
