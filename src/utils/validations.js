import * as yup from "yup";
import { parseNumber } from "libphonenumber-js";
import moment from "moment";

export const emailValidationSchema = yup
  .string()
  .required("Required")
  .email("Invalid format")
  .max(256, "Maximum ${max} chracters");

export const passwordValidationSchema = yup
  .string()
  .required("Required")
  .min(8, "Minimum ${min} characters")
  .max(256, "Maximum ${max} chracters");

export const passwordConfirmValidationSchema = yup
  .string()
  .oneOf([yup.ref("password"), null], "Passwords don't match")
  .required("Password confirm is required");

export const firstNameValidationSchema = yup
  .string()
  .max(256, "Maximum ${max} chracters");

export const lastNameValidationSchema = yup
  .string()
  .max(256, "Maximum ${max} chracters");

export const basicRequiredStringValidationSchema = yup
  .string()
  .required("Required")
  .max(256, "Maximum ${max} chracters");

export const zipValidationSchema = yup
  .string()
  .matches(/^\d{5}([ \-]\d{4})?$/, "Invalid zip code");

export const phoneValidationSchema = yup
  .string()
  .notRequired()
  .test("is-valid-phone-number", "Invalid format", value =>
    !!value ? !!parseNumber(value).country : true
  );

export const stringMaxValidationSchema = yup
  .string()
  .max(256, "Maximum ${max} chracters");

export const textMaxValidationSchema = yup
  .string()
  .max(1024, "Maximum ${max} chracters");

export const positiveIntegerValidationSchema = yup
  .number()
  .integer("Should be integer")
  .positive("Should be positive number")
  .required("Required")
  .max(Number.MAX_SAFE_INTEGER, "Maximum ${max} integer");

export const positiveNumberValidationSchema = yup
  .number()
  .positive("Should be positive number")
  .required("Required");

export const numberValidationSchema = yup.number().required("Required");

export const dateFutureValidationSchema = yup
  .date()
  .min(moment().format("YYYY-MM-DD"), "Should be in future");

export const afterDateValidationSchema = previous =>
  yup
    .date()
    .when(previous, st => yup.date().min(st, `Should after ${previous}`));
