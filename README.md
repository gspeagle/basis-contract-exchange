## Configuration

App use the firebase platform as database and backend layer. To configure 
project for specific firebase project should be changed next files:
    
 * [Firebase.js](/src/components/Firebase/Firebase.js) responsible for 
    frontend connection to firebase platform. There can be found firebase 
    config data: 
    
    ```javascript
        const config = {
          apiKey: "AIzaSyBE_RH6P3XwlTQ0uLAVg7XkwH4c75EftfA",
          authDomain: "test-63223.firebaseapp.com",
          databaseURL: "https://test-63223.firebaseio.com",
          projectId: "test-63223",
          storageBucket: "test-63223.appspot.com",
          messagingSenderId: "376816972692",
          appId: "1:376816972692:web:325a92d851d97a75"
        };
    ```

* [functions](/functions) folder contains firebase cloud function's 
(backend) files. To deploy this functions should be used ```firebase-tools```.
For this needs to run next commands from ```functions``` folder:
    Installation commands(should be run once): 
    ```shell
        npm install firebase-functions@latest firebase-admin@latest --save
        npm install -g firebase-tools
        firebase login 
    ```
    Then to deploy(should be run each time after updates):
    ```shell 
        firebase deploy --only functions
    ```
    
    [Get started example](https://firebase.google.com/docs/functions/get-started)

* [script](/script) folder contains contracts generation script. To run it 
needs to add [firebase service account key](https://firebase.google.com/docs/admin/setup#initialize_the_sdk).
The key should be in ```key.json``` file, in [script](/script) folder.
Command from ```script``` folder: 
    ```shell 
        node contracts.js
    ```
    It will start contracts generation.
    
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

