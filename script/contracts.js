import { ContractStatus } from "../src/constants/ContractStatusEnum";

const admin = require("firebase-admin");

const serviceAccount = require("./key.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://test-63223.firebaseio.com"
});
const MAX = 6700;
const MIN = 6200;

const templateData = {
  approvedOffer: "A43cPIVBxC6rlzrkoQLi",
  high: 6600,
  low: 6300,
  open: 6395,
  price: 6600,
  basis: 123,
  basisMonth: "July",
  cashPrice: 1,
  category: "Corn",
  commodityName: "Namename",
  deliveryDate: "2019-06-10",
  name: "Corn-20",
  location: "California",
  status: ContractStatus.ACCEPTED,
  uid: "VkyuSZB0sSbtHbTBNUNGoGd7zyq1"
};

const addContract = data =>
  admin
    .firestore()
    .collection("users")
    .doc(data.uid)
    .collection("contracts")
    .add({ ...data });

const random = (from, to) => Math.round(Math.random() * (to - from)) + from;

const generateContract = () => {
  const open = random(MIN, MAX);
  const high = random(MIN, MAX);
  const low = random(MIN, high);
  const price = random(low, high);
  return {
    ...templateData,
    high,
    low,
    open,
    price
  };
};

const emulator = async (i = 0) => {
  if (i > 20) {
    return;
  }
  const data = generateContract();
  const ref = await addContract(data);
  await setTimeout(async () => {
    await ref.update({
      status: ContractStatus.ACCEPTED,
      closedAt: { seconds: Math.ceil(new Date().getTime() / 1000) }
    });
    emulator(i + 1);
  }, 5000);
};
const timeout = ms => new Promise(resolve => setTimeout(resolve, ms));

const main = async () => {
  let i = 0;
  while (i < 30) {
    const data = generateContract();
    const ref = await addContract(data);
    const id = ref.id;
    await timeout(10000);
    await ref.update({
      status: ContractStatus.ACCEPTED,
      closedAt: { seconds: Math.ceil(new Date().getTime() / 1000) }
    });
    i++;
    console.log(`Added contract #${i}, ${id}`);
  }
};

main();
