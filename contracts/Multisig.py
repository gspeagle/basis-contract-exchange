#Multisig

import smartpy as sp

#Define multisig smart contract with initial parameters
class Multisig(sp.Contract):
    def __init__(self, _buyer, _seller, _admin, **kargs):
        self.init(buyer = sp.address(_buyer),
        seller = sp.address(_seller),
        admin = sp.address(_admin),
        destination = sp.none,
        tzamount = sp.tez(0),
        signed = sp.Set(),
        deposit = False,
        madeOffer = False,
        madeOffer_addr = sp.none,
        num_signed = 0,
        closed = False, **kargs)

    @sp.entryPoint
    def create_offer(self, params):
        #propose a trade only if you are in the address pool and an offer hasn't been made already
        sp.verify(self.data.madeOffer == False)
        sp.verify((self.data.buyer == sp.sender) | (self.data.seller == sp.sender) | (self.data.admin == sp.sender))
        #sets destination address and amount in Tezos
        self.data.destination = sp.Some(params.addr)
        self.data.tzamount = sp.tez(params.amt)
        #sets flag true indicating an offer is now open and who sent it
        self.data.madeOffer = True
        self.data.madeOffer_addr = sp.Some(sp.sender)

    @sp.entryPoint
    def deposit(self, params):
        #you can only deposit the amount in the offer
        sp.verify(self.data.deposit == False)
        sp.verify(self.data.madeOffer == True)
        sp.verify(self.data.madeOffer_addr.openSome() == sp.sender)
        sp.verify(sp.amount == sp.tez(params.amt))
        self.data.deposit = True

    @sp.entryPoint
    def delete_offer(self, params):
        #deletes current offer if you created the offer (admin has permissions)
        sp.verify(self.data.madeOffer == True)
        sp.verify((self.data.madeOffer_addr.openSome() == sp.sender) | (self.data.admin == sp.sender))
        #resets variables to initial values
        self.data.destination = sp.none
        self.data.tzamount = sp.tez(0)
        self.data.signed = sp.Set()
        self.data.num_signed = 0
        self.data.madeOffer = False
        self.data.closed = False
        self.data.deposit = False
        #tezos that was sent to the contract gets refunded to the address that initiated create_offer
        sp.send(self.data.madeOffer_addr.openSome(), sp.balance)
        #remove the address that made the offer
        self.data.madeOffer_addr = sp.none

    @sp.entryPoint
    def sign_offer(self, params):
        #signs the current offer with Tezos your address only if you haven't signed already and if an offer is open.
        sp.verify(self.data.madeOffer == True)
        sp.verify(self.data.deposit == True)
        sp.verify((self.data.buyer == sp.sender) | (self.data.seller == sp.sender) | (self.data.admin == sp.sender))
        sp.verify(~(self.data.signed.contains(sp.sender)))
        #adds your Tezos address to signed list and adds vote to a counter
        self.data.signed.add(sp.sender)
        self.data.num_signed += 1

    @sp.entryPoint
    def execute_offer(self, params):
        #Sends signed Tezos transaction to destination address with amount
        #checks if offer is open, you're in the addrpool, the num of signatures exceeds 2, and an offer hasn't been closed recently.
        sp.verify(self.data.madeOffer == True)
        sp.verify(self.data.deposit == True)
        sp.verify((self.data.madeOffer_addr.openSome() == sp.sender) | (self.data.admin == sp.sender))
        sp.verify(self.data.num_signed >= 2)
        sp.verify(self.data.closed == False)
        #the offer is now closed. lastly send Tezos.
        self.data.closed = True
        #sends Tezos to destination address for specified amount
        sp.send(self.data.destination.openSome(), self.data.tzamount)


    @addTest(name = "Multisig Test")
    def test():
        html = ""
        test_admin = "tz1kdm5eUHAHG93MTZ7G7yCaRY9XVCa8fpki"
        test_seller = "tz1TYhmuZw5gEbhMikiM8eMCK9UVU1GRPS3Q"
        test_buyer = "tz1hb8XRoHAV5BkjFZ7G7yCaRY9XVCa8fhmd"
        c1 = Multisig(test_buyer, test_seller, test_admin)
        html += c1.fullHtml()
        html += c1.create_offer(addr = sp.address("tz1kdm5eUHAHG93MTZ7G7yCaRY9XVCa8foos"), amt = 100).run(sender=test_admin).html()
        html += c1.deposit(amt = 100).run(sender = test_admin, amount = sp.tez(100)).html()
        html += c1.delete_offer().run(sender=test_admin).html()
        html += c1.create_offer(addr = sp.address("tz1kdm5eUHAHG93MTZ7G7yCaRY9XVCa8fpki"), amt = 100000).run(sender=test_seller).html()
        html += c1.deposit(amt = 100000).run(sender = test_seller, amount = sp.tez(100000)).html()
        html += c1.sign_offer().run(sender=test_admin).html()
        html += c1.sign_offer().run(sender=test_seller).html()
        #html += c1.sign_offer().run(sender=test_admin).html()
        html += c1.execute_offer().run(sender=test_seller).html()
        setOutput(html)
