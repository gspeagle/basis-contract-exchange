import smartpy as sp

class Escrow(sp.Contract):
    def __init__(self, elevator, farmer, bushels, basis, future_price, total_to_pay, delivery_date):
        self.init(basis               = basis,
                  bushels             = bushels,
                  future_price        = future_price,
                  delivery_date       = delivery_date,
                  total_to_pay        = total_to_pay,
                  elevator            = sp.address(elevator),
                  farmer              = sp.address(farmer),
                  deposited           = False,
                  delivery_confirmed  = False,
                  closed              = False)

    @sp.entryPoint
    def test(self, params):
        self.data.delivery_confirmed = params.test


    @sp.entryPoint
    def confirm_delivery(self, params):
        sp.verify(sp.sender == self.data.farmer)
        sp.verify(~self.data.delivery_confirmed)
        sp.verify(self.data.deposited)
        sp.verify(~self.data.closed)
        self.data.delivery_confirmed = True

    @sp.entryPoint
    def send_money_to_elevator(self, params):
        sp.verify(self.data.delivery_date < sp.currentTime)
        sp.verify(self.data.delivery_confirmed)
        sp.verify(self.data.deposited)
        sp.verify(sp.sender == self.data.elevator)
        sp.verify(~self.data.closed)
        sp.send(self.data.elevator, self.data.total_to_pay)
        self.data.closed = True

    @sp.entryPoint
    def deposit(self, params):
        sp.verify(~self.data.closed)
        sp.verify(sp.sender == self.data.farmer)
        sp.verify(self.data.total_to_pay == sp.amount)
        self.data.deposited = True

@addTest(name = "Test Escrow")
def test():
    bushels = 5000
    future_price = 40
    basis = 2
    total_to_pay = sp.tez(bushels * (future_price - basis ))
    elevator = "tz1TYhmuZw5gEbhMikiM8eMCK9UVU1GRPS3Q"
    farmer = "tz1hb8XRoHAV5BkjFZ7G7yCaRY9XVCa8fhmd"
    c1 = Escrow(elevator, farmer, bushels, basis, future_price, total_to_pay, sp.timestamp(12))
    html  = c1.fullHtml()
    html += h3("Wrong deposit amount")
    html += c1.deposit().run(sender = farmer, amount = sp.tez(bushels * (future_price - basis ) - 1)).html()
    html += h3("Correct deposit amount")
    html += c1.deposit().run(sender = farmer, amount = total_to_pay).html()
    html += h3("Should not confirm delivery if sender not farmer")
    html += c1.confirm_delivery().run(sender = elevator).html()
    html += h3("Should confirm delivery")
    html += c1.confirm_delivery().run(sender = farmer).html()
    html += h3("Should not send money to elevator if sender not elevator")
    html += c1.send_money_to_elevator().run(sender = farmer).html()
    html += h3("Should send money to elevator")
    html += c1.send_money_to_elevator().run(sender = elevator).html()
    setOutput(html)

